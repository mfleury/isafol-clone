# First-Order Logic According to Berghofer #

[This directory](https://bitbucket.org/isafol/isafol/src/master/FOL_Berghofer/) contains an ongoing Isabelle formalization of
first-order logic based on Stefan Berghofer�s [AFP entry](https://www.isa-afp.org/entries/FOL-Fitting.shtml).

Natural Deduction and the Isabelle Proof Assistant.  
J�rgen Villadsen, Andreas Halkj�r From and Anders Schlichtkrull.  
Post-proceedings of the 6th International Workshop on Theorem proving components for Educational software (ThEdu'17).  
[http://eptcs.org/content.cgi?ThEdu17](http://eptcs.org/content.cgi?ThEdu17)

Natural Deduction Assistant (NaDeA).  
J�rgen Villadsen, Andreas Halkj�r From and Anders Schlichtkrull.  
Post-proceedings of the 7th International Workshop on Theorem proving components for Educational software (ThEdu'18).  
[http://eptcs.org/content.cgi?thedu18](http://eptcs.org/content.cgi?thedu18)

## Author ##

* [Andreas Halkj�r From](mailto:s144442 shtrudel student.dtu.dk)

Contributors: Alexander Birch Jensen, Anders Schlichtkrull & J�rgen Villadsen
