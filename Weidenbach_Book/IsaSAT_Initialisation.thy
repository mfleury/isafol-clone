theory IsaSAT_Initialisation
  imports Watched_Literals.Watched_Literals_Watch_List_Initialisation IsaSAT_Setup IsaSAT_VMTF
    Automatic_Refinement.Relators \<comment> \<open>for more lemmas\<close>
begin
(*TODO Move*)

lemma fold_eq_nfoldli:
  "RETURN (fold f l s) = nfoldli l (\<lambda>_. True) (\<lambda>x s. RETURN (f x s)) s"
  apply (induction l arbitrary: s) apply (auto) done

(*End Move*)

no_notation Ref.update ("_ := _" 62)
hide_const Autoref_Fix_Rel.CONSTRAINT

section \<open>Code for the initialisation of the Data Structure\<close>

text \<open>The initialisation is done in three different steps:
  \<^enum> First, we extract all the atoms that appear in the problem and initialise the state with empty values.
    This part is called \<^emph>\<open>initialisation\<close> below.
  \<^enum> Then, we go over all clauses and insert them in our memory module. We call this phase \<^emph>\<open>parsing\<close>.
  \<^enum> Finally, we calculate the watch list.

Splitting the second from the third step makes it easier to add preprocessing and more important
to add a bounded mode.
\<close>

subsection \<open>Initialisation of the state\<close>

definition (in -) atoms_hash_empty where
 [simp]: \<open>atoms_hash_empty _ = {}\<close>


definition (in -) atoms_hash_int_empty where
  \<open>atoms_hash_int_empty n = RETURN (replicate n False)\<close>

lemma atoms_hash_int_empty_atoms_hash_empty:
  \<open>(atoms_hash_int_empty, RETURN o atoms_hash_empty) \<in>
   [\<lambda>n. (\<forall>L\<in>#\<L>\<^sub>a\<^sub>l\<^sub>l \<A>. atm_of L < n)]\<^sub>f nat_rel \<rightarrow> \<langle>atoms_hash_rel \<A>\<rangle>nres_rel\<close>
  by (intro frefI nres_relI)
    (use Max_less_iff in \<open>auto simp: atoms_hash_rel_def atoms_hash_int_empty_def atoms_hash_empty_def
      in_\<L>\<^sub>a\<^sub>l\<^sub>l_atm_of_\<A>\<^sub>i\<^sub>n in_\<L>\<^sub>a\<^sub>l\<^sub>l_atm_of_in_atms_of_iff Ball_def
      dest: spec[of _ "Pos _"]\<close>)

definition (in -) distinct_atms_empty where
  \<open>distinct_atms_empty _ = {}\<close>

definition (in -) distinct_atms_int_empty where
  \<open>distinct_atms_int_empty n = RETURN ([], replicate n False)\<close>


lemma distinct_atms_int_empty_distinct_atms_empty:
  \<open>(distinct_atms_int_empty, RETURN o distinct_atms_empty) \<in>
     [\<lambda>n. (\<forall>L\<in>#\<L>\<^sub>a\<^sub>l\<^sub>l \<A>. atm_of L < n)]\<^sub>f nat_rel \<rightarrow> \<langle>distinct_atoms_rel \<A>\<rangle>nres_rel\<close>
  apply (intro frefI nres_relI)
  apply (auto simp: distinct_atoms_rel_alt_def distinct_atms_empty_def distinct_atms_int_empty_def)
  by (metis atms_of_\<L>\<^sub>a\<^sub>l\<^sub>l_\<A>\<^sub>i\<^sub>n atms_of_def imageE)

type_synonym vmtf_remove_int_option_fst_As = \<open>vmtf_option_fst_As \<times> nat set\<close>

type_synonym isa_vmtf_remove_int_option_fst_As = \<open>vmtf_option_fst_As \<times> nat list \<times> bool list\<close>

definition vmtf_init
   :: \<open>nat multiset \<Rightarrow> (nat, nat) ann_lits \<Rightarrow> vmtf_remove_int_option_fst_As set\<close>
where
  \<open>vmtf_init \<A>\<^sub>i\<^sub>n M = {((ns, m, fst_As, lst_As, next_search), to_remove).
   \<A>\<^sub>i\<^sub>n \<noteq> {#} \<longrightarrow> (fst_As \<noteq> None \<and> lst_As \<noteq> None \<and> ((ns, m, the fst_As, the lst_As, next_search),
     to_remove) \<in> vmtf \<A>\<^sub>i\<^sub>n M)}\<close>

definition isa_vmtf_init where
  \<open>isa_vmtf_init \<A> M =
    ((Id \<times>\<^sub>r nat_rel \<times>\<^sub>r \<langle>nat_rel\<rangle>option_rel \<times>\<^sub>r \<langle>nat_rel\<rangle>option_rel \<times>\<^sub>r \<langle>nat_rel\<rangle>option_rel) \<times>\<^sub>f
        distinct_atoms_rel \<A>)\<inverse>
      `` vmtf_init \<A> M\<close>

lemma isa_vmtf_initI:
  \<open>(vm, to_remove') \<in> vmtf_init \<A> M \<Longrightarrow> (to_remove, to_remove') \<in> distinct_atoms_rel \<A> \<Longrightarrow>
    (vm, to_remove) \<in> isa_vmtf_init \<A> M\<close>
  by (auto simp: isa_vmtf_init_def Image_iff intro!: bexI[of _ \<open>(vm, to_remove')\<close>])

lemma isa_vmtf_init_consD:
  \<open>((ns, m, fst_As, lst_As, next_search), remove) \<in> isa_vmtf_init \<A> M \<Longrightarrow>
     ((ns, m, fst_As, lst_As, next_search), remove) \<in> isa_vmtf_init \<A> (L # M)\<close>
  by (auto simp: isa_vmtf_init_def vmtf_init_def dest: vmtf_consD)

lemma vmtf_init_cong:
  \<open>set_mset \<A> = set_mset \<B> \<Longrightarrow> L \<in> vmtf_init \<A> M \<Longrightarrow> L \<in> vmtf_init \<B> M\<close>
  using \<L>\<^sub>a\<^sub>l\<^sub>l_cong[of \<A> \<B>] atms_of_\<L>\<^sub>a\<^sub>l\<^sub>l_cong[of \<A> \<B>] vmtf_cong[of \<A> \<B>]
  unfolding vmtf_init_def vmtf_\<L>\<^sub>a\<^sub>l\<^sub>l_def
  by auto

lemma isa_vmtf_init_cong:
  \<open>set_mset \<A> = set_mset \<B> \<Longrightarrow> L \<in> isa_vmtf_init \<A> M \<Longrightarrow> L \<in> isa_vmtf_init \<B> M\<close>
  using vmtf_init_cong[of \<A> \<B>] distinct_atoms_rel_cong[of \<A> \<B>]
  apply (subst (asm) isa_vmtf_init_def)
  by (cases L) (auto intro!: isa_vmtf_initI)

type_synonym vdom_fast = \<open>uint64 list\<close>

type_synonym (in -) twl_st_wl_heur_init =
  \<open>trail_pol \<times> arena \<times> conflict_option_rel \<times> nat \<times>
    (nat \<times> nat literal \<times> bool) list list \<times> isa_vmtf_remove_int_option_fst_As \<times> bool list \<times>
    nat \<times> conflict_min_cach_l \<times> lbd \<times> vdom \<times> bool\<close>

type_synonym (in -) twl_st_wl_heur_init_full =
  \<open>trail_pol \<times> arena \<times> conflict_option_rel \<times> nat \<times>
    (nat \<times> nat literal \<times> bool) list list \<times> isa_vmtf_remove_int_option_fst_As \<times> bool list \<times>
    nat \<times> conflict_min_cach_l \<times> lbd \<times> vdom \<times> bool\<close>


text \<open>The initialisation relation is stricter in the sense that it already includes the relation
of atom inclusion.

Remark that we replace \<^term>\<open>(D = None \<longrightarrow> j \<le> length M)\<close> by \<^term>\<open>j \<le> length M\<close>: this simplifies the
proofs and does not make a difference in the generated code, since there are no conflict analysis
at that level anyway.
\<close>
text \<open>KILL duplicates below, but difference:
  vmtf vs vmtf\_init
  watch list vs no WL
  OC vs non-OC
  \<close>
definition twl_st_heur_parsing_no_WL
  :: \<open>nat multiset \<Rightarrow> bool \<Rightarrow> (twl_st_wl_heur_init \<times> nat twl_st_wl_init) set\<close>
where
\<open>twl_st_heur_parsing_no_WL \<A> unbdd =
  {((M', N', D', j, W', vm, \<phi>, clvls, cach, lbd, vdom, failed), ((M, N, D, NE, UE, Q), OC)).
    (unbdd \<longrightarrow> \<not>failed) \<and>
    ((unbdd \<or> \<not>failed) \<longrightarrow>
     (valid_arena N' N (set vdom) \<and>
      set_mset
       (all_lits_of_mm
          ({#mset (fst x). x \<in># ran_m N#} + NE + UE)) \<subseteq> set_mset (\<L>\<^sub>a\<^sub>l\<^sub>l \<A>) \<and>
       mset vdom = dom_m N)) \<and>
    (M', M) \<in> trail_pol \<A> \<and>
    (D',  D) \<in> option_lookup_clause_rel \<A> \<and>
    j \<le> length M \<and>
    Q = uminus `# lit_of `# mset (drop j (rev M)) \<and>
    vm \<in> isa_vmtf_init \<A> M \<and>
    phase_saving \<A> \<phi> \<and>
    no_dup M \<and>
    cach_refinement_empty \<A> cach \<and>
    (W', empty_watched \<A>) \<in> \<langle>Id\<rangle>map_fun_rel (D\<^sub>0 \<A>) \<and>
    isasat_input_bounded \<A> \<and>
    distinct vdom
  }\<close>


definition twl_st_heur_parsing
  :: \<open>nat multiset \<Rightarrow> bool \<Rightarrow> (twl_st_wl_heur_init \<times> (nat twl_st_wl \<times> nat clauses)) set\<close>
where
\<open>twl_st_heur_parsing \<A>  unbdd =
  {((M', N', D', j, W', vm, \<phi>, clvls, cach, lbd, vdom, failed), ((M, N, D, NE, UE, Q, W), OC)).
    (unbdd \<longrightarrow> \<not>failed) \<and>
    ((unbdd \<or> \<not>failed) \<longrightarrow>
    ((M', M) \<in> trail_pol \<A> \<and>
    valid_arena N' N (set vdom) \<and>
    (D',  D) \<in> option_lookup_clause_rel \<A> \<and>
    j \<le> length M \<and>
    Q = uminus `# lit_of `# mset (drop j (rev M)) \<and>
    vm \<in> isa_vmtf_init \<A> M \<and>
    phase_saving \<A> \<phi> \<and>
    no_dup M \<and>
    cach_refinement_empty \<A> cach \<and>
    mset vdom = dom_m N \<and>
    vdom_m \<A> W N = set_mset (dom_m N) \<and>
    set_mset
     (all_lits_of_mm
       ({#mset (fst x). x \<in># ran_m N#} + NE + UE)) \<subseteq> set_mset (\<L>\<^sub>a\<^sub>l\<^sub>l \<A>) \<and>
    (W', W) \<in> \<langle>Id\<rangle>map_fun_rel (D\<^sub>0  \<A>) \<and>
    isasat_input_bounded \<A> \<and>
    distinct vdom))
  }\<close>


definition twl_st_heur_parsing_no_WL_wl :: \<open>nat multiset \<Rightarrow> bool \<Rightarrow> (_ \<times> nat twl_st_wl_init') set\<close> where
\<open>twl_st_heur_parsing_no_WL_wl \<A>  unbdd =
  {((M', N', D', j, W', vm, \<phi>, clvls, cach, lbd, vdom, failed), (M, N, D, NE, UE, Q)).
    (unbdd \<longrightarrow> \<not>failed) \<and>
    ((unbdd \<or> \<not>failed) \<longrightarrow>
      (valid_arena N' N (set vdom) \<and> set_mset (dom_m N) \<subseteq> set vdom)) \<and>
    (M', M) \<in> trail_pol \<A> \<and>
    (D', D) \<in> option_lookup_clause_rel \<A> \<and>
    j \<le> length M \<and>
    Q = uminus `# lit_of `# mset (drop j (rev M)) \<and>
    vm \<in> isa_vmtf_init \<A> M \<and>
    phase_saving \<A> \<phi> \<and>
    no_dup M \<and>
    cach_refinement_empty \<A> cach \<and>
    set_mset (all_lits_of_mm ({#mset (fst x). x \<in># ran_m N#} + NE + UE))
      \<subseteq> set_mset (\<L>\<^sub>a\<^sub>l\<^sub>l \<A>) \<and>
    (W', empty_watched \<A>) \<in> \<langle>Id\<rangle>map_fun_rel (D\<^sub>0 \<A>) \<and>
    isasat_input_bounded \<A> \<and>
    distinct vdom
  }\<close>

definition twl_st_heur_parsing_no_WL_wl_no_watched :: \<open>nat multiset \<Rightarrow> bool \<Rightarrow> (twl_st_wl_heur_init_full \<times> nat twl_st_wl_init) set\<close> where
\<open>twl_st_heur_parsing_no_WL_wl_no_watched \<A> unbdd =
  {((M', N', D', j, W', vm, \<phi>, clvls, cach, lbd, vdom, failed), ((M, N, D, NE, UE, Q), OC)).
    (unbdd \<longrightarrow> \<not>failed) \<and>
    ((unbdd \<or> \<not>failed) \<longrightarrow>
      (valid_arena N' N (set vdom) \<and> set_mset (dom_m N) \<subseteq> set vdom)) \<and> (M', M) \<in> trail_pol \<A> \<and>
    (D', D) \<in> option_lookup_clause_rel \<A> \<and>
    j \<le> length M \<and>
    Q = uminus `# lit_of `# mset (drop j (rev M)) \<and>
    vm \<in> isa_vmtf_init \<A> M \<and>
    phase_saving \<A> \<phi> \<and>
    no_dup M \<and>
    cach_refinement_empty \<A> cach \<and>
    set_mset (all_lits_of_mm ({#mset (fst x). x \<in># ran_m N#} + NE + UE))
       \<subseteq> set_mset (\<L>\<^sub>a\<^sub>l\<^sub>l \<A>) \<and>
    (W', empty_watched \<A>) \<in> \<langle>Id\<rangle>map_fun_rel (D\<^sub>0 \<A>) \<and>
    isasat_input_bounded \<A> \<and>
    distinct vdom
  }\<close>

definition twl_st_heur_post_parsing_wl :: \<open>bool \<Rightarrow> (twl_st_wl_heur_init_full \<times> nat twl_st_wl) set\<close> where
\<open>twl_st_heur_post_parsing_wl unbdd =
  {((M', N', D', j, W', vm, \<phi>, clvls, cach, lbd, vdom, failed), (M, N, D, NE, UE, Q, W)).
    (unbdd \<longrightarrow> \<not>failed) \<and>
    ((unbdd \<or> \<not>failed) \<longrightarrow>
     ((M', M) \<in> trail_pol (all_atms N (NE + UE)) \<and>
      set_mset (dom_m N) \<subseteq> set vdom \<and>
      valid_arena N' N (set vdom))) \<and>
    (D', D) \<in> option_lookup_clause_rel (all_atms N (NE + UE)) \<and>
    j \<le> length M \<and>
    Q = uminus `# lit_of `# mset (drop j (rev M)) \<and>
    vm \<in> isa_vmtf_init (all_atms N (NE + UE)) M \<and>
    phase_saving (all_atms N (NE + UE)) \<phi> \<and>
    no_dup M \<and>
    cach_refinement_empty (all_atms N (NE + UE)) cach \<and>
    vdom_m (all_atms N (NE + UE)) W N \<subseteq> set vdom \<and>
    set_mset (all_lits_of_mm ({#mset (fst x). x \<in># ran_m N#} + NE + UE))
      \<subseteq> set_mset (\<L>\<^sub>a\<^sub>l\<^sub>l (all_atms N (NE + UE))) \<and>
    (W', W) \<in> \<langle>Id\<rangle>map_fun_rel (D\<^sub>0 (all_atms N (NE + UE))) \<and>
    isasat_input_bounded (all_atms N (NE + UE)) \<and>
    distinct vdom
  }\<close>


subsubsection \<open>VMTF\<close>

definition initialise_VMTF :: \<open>uint32 list \<Rightarrow> nat \<Rightarrow> isa_vmtf_remove_int_option_fst_As nres\<close> where
\<open>initialise_VMTF N n = do {
   let A = replicate n (VMTF_Node zero_uint64_nat None None);
   to_remove \<leftarrow> distinct_atms_int_empty n;
   ASSERT(length N \<le> uint32_max);
   (n, A, cnext) \<leftarrow> WHILE\<^sub>T
      (\<lambda>(i, A, cnext). i < length_uint32_nat N)
      (\<lambda>(i, A, cnext). do {
        ASSERT(i < length_uint32_nat N);
        let L = nat_of_uint32 (N ! i);
        ASSERT(L < length A);
        ASSERT(cnext \<noteq> None \<longrightarrow> the cnext < length A);
        ASSERT(i + 1 \<le> uint_max);
        RETURN (i + one_uint32_nat, vmtf_cons A L cnext (uint64_of_uint32_conv i), Some L)
      })
      (zero_uint32_nat, A, None);
   RETURN ((A, uint64_of_uint32_conv n, cnext, (if N = [] then None else Some (nat_of_uint32 (N!0))), cnext), to_remove)
  }\<close>


lemma initialise_VMTF:
  shows  \<open>(uncurry initialise_VMTF, uncurry (\<lambda>N n. RES (vmtf_init N []))) \<in>
      [\<lambda>(N,n). (\<forall>L\<in># N. L < n) \<and> (distinct_mset N) \<and> size N < uint32_max \<and> set_mset N = set_mset \<A>]\<^sub>f
      (\<langle>uint32_nat_rel\<rangle>list_rel_mset_rel) \<times>\<^sub>f nat_rel \<rightarrow>
      \<langle>(\<langle>Id\<rangle>list_rel \<times>\<^sub>r nat_rel \<times>\<^sub>r \<langle>nat_rel\<rangle> option_rel \<times>\<^sub>r \<langle>nat_rel\<rangle> option_rel \<times>\<^sub>r \<langle>nat_rel\<rangle> option_rel)
        \<times>\<^sub>r distinct_atoms_rel \<A>\<rangle>nres_rel\<close>
    (is \<open>(?init, ?R) \<in> _\<close>)
proof -
  have vmtf_ns_notin_empty: \<open>vmtf_ns_notin [] 0 (replicate n (VMTF_Node 0 None None))\<close> for n
    unfolding vmtf_ns_notin_def
    by auto

  have K2: \<open>distinct N \<Longrightarrow> fst_As < length N \<Longrightarrow> N!fst_As \<in> set (take fst_As N) \<Longrightarrow> False\<close>
    for fst_As x N
    by (metis (no_types, lifting) in_set_conv_nth length_take less_not_refl min_less_iff_conj
      nth_eq_iff_index_eq nth_take)
  have W_ref: \<open>WHILE\<^sub>T (\<lambda>(i, A, cnext). i < length_uint32_nat N')
        (\<lambda>(i, A, cnext). do {
              _ \<leftarrow> ASSERT (i < length_uint32_nat N');
              let L = nat_of_uint32 (N' ! i);
              _ \<leftarrow> ASSERT (L < length A);
              _ \<leftarrow> ASSERT (cnext \<noteq> None \<longrightarrow> the cnext < length A);
              _ \<leftarrow> ASSERT (i + 1 \<le> uint_max);
              RETURN
               (i + one_uint32_nat,
                vmtf_cons A L cnext (uint64_of_uint32_conv i), Some L)
            })
        (zero_uint32_nat, replicate n' (VMTF_Node zero_uint64_nat None None),
         None)
    \<le> SPEC(\<lambda>(i, A', cnext).
      vmtf_ns (rev (map (nat_of_uint32) (take i N'))) i A'
        \<and> cnext = map_option (nat_of_uint32) (option_last (take i N')) \<and>  i = length N' \<and>
          length A' = n \<and> vmtf_ns_notin (rev (map (nat_of_uint32) (take i N'))) i A'
      )\<close>
    (is \<open>_ \<le> SPEC ?P\<close>)
    if H: \<open>case y of (N, n) \<Rightarrow>(\<forall>L\<in># N. L < n) \<and> distinct_mset N \<and> size N < uint32_max \<and>
         set_mset N = set_mset \<A>\<close> and
       ref: \<open>(x, y) \<in> \<langle>uint32_nat_rel\<rangle>list_rel_mset_rel \<times>\<^sub>f nat_rel\<close> and
       st[simp]: \<open>x = (N', n')\<close> \<open>y = (N, n)\<close>
     for N N' n n' A x y
  proof -
  have [simp]: \<open>n = n'\<close> and NN': \<open>(N', N) \<in> \<langle>uint32_nat_rel\<rangle>list_rel_mset_rel\<close>
    using ref unfolding st by auto
  have \<open>inj_on nat_of_uint32 S\<close> for S
    by (auto simp: inj_on_def)
  then have dist: \<open>distinct N'\<close>
    using NN' H by (auto simp: list_rel_def uint32_nat_rel_def br_def list_mset_rel_def
      list_all2_op_eq_map_right_iff' distinct_image_mset_inj list_rel_mset_rel_def)
  have L_N: \<open>\<forall>L\<in>set N'. nat_of_uint32 L < n\<close>
    using H ref by (auto simp: list_rel_def uint32_nat_rel_def br_def list_mset_rel_def
      list_all2_op_eq_map_right_iff' list_rel_mset_rel_def)
  let ?Q = \<open>\<lambda>(i, A', cnext).
      vmtf_ns (rev (map (nat_of_uint32) (take i N'))) i A' \<and> i \<le> length N' \<and>
      cnext = map_option (nat_of_uint32) (option_last (take i N')) \<and>
      length A' = n \<and> vmtf_ns_notin (rev (map (nat_of_uint32) (take i N'))) i A'\<close>
  show ?thesis
    apply (refine_vcg WHILET_rule[where R = \<open>measure (\<lambda>(i, _). length N' + 1 - i)\<close> and I = \<open>?Q\<close>])
    subgoal by auto
    subgoal by (auto intro: vmtf_ns.intros)
    subgoal by auto
    subgoal by auto
    subgoal by auto
    subgoal for S N' x2 A'
      unfolding assert_bind_spec_conv vmtf_ns_notin_def
      using L_N dist
      by (auto 5 5 simp: take_Suc_conv_app_nth hd_drop_conv_nth nat_shiftr_div2 nat_of_uint32_shiftr
          option_last_def hd_rev last_map intro!: vmtf_cons dest: K2)
    subgoal by auto
    subgoal
      using L_N dist
      by (auto simp: take_Suc_conv_app_nth hd_drop_conv_nth nat_shiftr_div2 nat_of_uint32_shiftr
          option_last_def hd_rev last_map)
    subgoal
      using L_N dist
      by (auto simp: last_take_nth_conv option_last_def)
    subgoal
      using H dist ref
      by (auto simp: last_take_nth_conv option_last_def list_rel_mset_rel_imp_same_length)
    subgoal
      using L_N dist
      by (auto 5 5 simp: take_Suc_conv_app_nth option_last_def hd_rev last_map intro!: vmtf_cons
          dest: K2)
    subgoal by (auto simp: take_Suc_conv_app_nth)
    subgoal by (auto simp: take_Suc_conv_app_nth)
    subgoal by auto
    subgoal
      using L_N dist
      by (auto 5 5 simp: take_Suc_conv_app_nth hd_rev last_map option_last_def
          intro!: vmtf_notin_vmtf_cons dest: K2 split: if_splits)
    subgoal by auto
    subgoal by auto
    subgoal by auto
    subgoal by auto
    subgoal by auto
    subgoal by auto
    done
  qed
  have [simp]: \<open>vmtf_\<L>\<^sub>a\<^sub>l\<^sub>l n' [] ((nat_of_uint32 ` set N, {}), {})\<close>
    if \<open>(N, n') \<in> \<langle>uint32_nat_rel\<rangle>list_rel_mset_rel\<close> for N N' n'
    using that unfolding vmtf_\<L>\<^sub>a\<^sub>l\<^sub>l_def
    by (auto simp: \<L>\<^sub>a\<^sub>l\<^sub>l_def atms_of_def image_image image_Un list_rel_def
      uint32_nat_rel_def br_def list_mset_rel_def list_all2_op_eq_map_right_iff'
      list_rel_mset_rel_def)
  have in_N_in_N1: \<open>L \<in> set N' \<Longrightarrow>  nat_of_uint32 L \<in> atms_of (\<L>\<^sub>a\<^sub>l\<^sub>l N)\<close>
    if \<open>(N', y) \<in> \<langle>uint32_nat_rel\<rangle>list_rel\<close> and \<open>(y, N) \<in> list_mset_rel\<close> for L N N' y
    using that by (auto simp: \<L>\<^sub>a\<^sub>l\<^sub>l_def atms_of_def image_image image_Un list_rel_def
      uint32_nat_rel_def br_def list_mset_rel_def list_all2_op_eq_map_right_iff')

  have length_ba: \<open>\<forall>L\<in># N. L < length ba \<Longrightarrow> L \<in> atms_of (\<L>\<^sub>a\<^sub>l\<^sub>l N) \<Longrightarrow>
     L < length ba\<close>
    if \<open>(N', y) \<in> \<langle>uint32_nat_rel\<rangle>list_rel_mset_rel\<close>
    for L ba N N' y
    using that
    by (auto simp: \<L>\<^sub>a\<^sub>l\<^sub>l_def nat_shiftr_div2 nat_of_uint32_shiftr
      atms_of_def image_image image_Un split: if_splits)
  show ?thesis
    apply (intro frefI nres_relI)
    unfolding initialise_VMTF_def uncurry_def conc_Id id_def vmtf_init_def
      distinct_atms_int_empty_def nres_monad1
    apply (refine_rcg)
   subgoal by (auto dest: list_rel_mset_rel_imp_same_length)
    apply (rule specify_left)
     apply (rule W_ref; assumption?)
    subgoal for N' N'n' n' Nn N n st
      apply (case_tac st)
      apply clarify
      apply (subst RETURN_RES_refine_iff)
      apply (auto dest: list_rel_mset_rel_imp_same_length)
      apply (rule exI[of _ \<open>{}\<close>])
      apply (auto simp: distinct_atoms_rel_alt_def list_rel_mset_rel_def list_mset_rel_def
        br_def; fail)
      apply (rule exI[of _ \<open>{}\<close>])
      unfolding vmtf_def in_pair_collect_simp prod.case
      apply (intro conjI impI)
      apply (rule exI[of _ \<open>map nat_of_uint32 (rev (fst N'))\<close>])
      apply (rule_tac exI[of _ \<open>[]\<close>])
      apply (intro conjI impI)
      subgoal
        by (auto simp: rev_map[symmetric] vmtf_def option_last_def last_map
            hd_rev list_rel_mset_rel_def br_def list_mset_rel_def)
     (* subgoal apply (auto simp: rev_map[symmetric] vmtf_def option_hd_rev
            map_option_option_last)*)
      subgoal by (auto simp: rev_map[symmetric] vmtf_def option_hd_rev
            map_option_option_last hd_map hd_conv_nth rev_nth last_conv_nth
	    list_rel_mset_rel_def br_def list_mset_rel_def)
      subgoal by (auto simp: rev_map[symmetric] vmtf_def option_hd_rev
            map_option_option_last hd_map last_map hd_conv_nth rev_nth last_conv_nth
	    list_rel_mset_rel_def br_def list_mset_rel_def)
      subgoal by (auto simp: rev_map[symmetric] vmtf_def option_hd_rev
            map_option_option_last hd_rev last_map distinct_atms_empty_def)
      subgoal by (auto simp: rev_map[symmetric] vmtf_def option_hd_rev
            map_option_option_last list_rel_mset_rel_def)
      subgoal by (auto simp: rev_map[symmetric] vmtf_def option_hd_rev
            map_option_option_last dest: length_ba)
      subgoal by (auto simp: rev_map[symmetric] vmtf_def option_hd_rev
            map_option_option_last hd_map hd_conv_nth rev_nth last_conv_nth
	    list_rel_mset_rel_def br_def list_mset_rel_def atms_of_\<L>\<^sub>a\<^sub>l\<^sub>l_\<A>\<^sub>i\<^sub>n)
      subgoal by (auto simp: rev_map[symmetric] vmtf_def option_hd_rev
            map_option_option_last list_rel_mset_rel_def dest: in_N_in_N1)
      subgoal by (auto simp: distinct_atoms_rel_alt_def list_rel_mset_rel_def list_mset_rel_def
        br_def)
      done
    done
qed


subsection \<open>Parsing\<close>

fun (in -)get_conflict_wl_heur_init :: \<open>twl_st_wl_heur_init \<Rightarrow> conflict_option_rel\<close> where
  \<open>get_conflict_wl_heur_init (_, _, D, _) = D\<close>

fun (in -)get_clauses_wl_heur_init :: \<open>twl_st_wl_heur_init \<Rightarrow> arena\<close> where
  \<open>get_clauses_wl_heur_init (_, N, _) = N\<close>

fun (in -) get_trail_wl_heur_init :: \<open>twl_st_wl_heur_init \<Rightarrow> trail_pol\<close> where
  \<open>get_trail_wl_heur_init (M, _, _, _, _, _, _) = M\<close>

fun (in -) get_vdom_heur_init :: \<open>twl_st_wl_heur_init \<Rightarrow> nat list\<close> where
  \<open>get_vdom_heur_init (_, _, _, _, _, _, _, _, _, _, vdom, _) = vdom\<close>

fun (in -) is_failed_heur_init :: \<open>twl_st_wl_heur_init \<Rightarrow> bool\<close> where
  \<open>is_failed_heur_init (_, _, _, _, _, _, _, _, _, _, _, failed) = failed\<close>

definition propagate_unit_cls
  :: \<open>nat literal \<Rightarrow> nat twl_st_wl_init \<Rightarrow> nat twl_st_wl_init\<close>
where
  \<open>propagate_unit_cls = (\<lambda>L ((M, N, D, NE, UE, Q), OC).
     ((Propagated L 0 # M, N, D, add_mset {#L#} NE, UE, Q), OC))\<close>

definition propagate_unit_cls_heur
 :: \<open>nat literal \<Rightarrow> twl_st_wl_heur_init \<Rightarrow> twl_st_wl_heur_init nres\<close>
where
  \<open>propagate_unit_cls_heur = (\<lambda>L (M, N, D, Q). do {
     ASSERT(cons_trail_Propagated_tr_pre ((L, 0 :: nat), M));
     RETURN (cons_trail_Propagated_tr L 0 M, N, D, Q)})\<close>

fun get_unit_clauses_init_wl :: \<open>'v twl_st_wl_init \<Rightarrow> 'v clauses\<close> where
  \<open>get_unit_clauses_init_wl ((M, N, D, NE, UE, Q), OC) = NE + UE\<close>


abbreviation all_lits_st_init :: \<open>'v twl_st_wl_init \<Rightarrow> 'v literal multiset\<close> where
  \<open>all_lits_st_init S \<equiv> all_lits (get_clauses_init_wl S) (get_unit_clauses_init_wl S)\<close>

definition all_atms_init :: \<open>_ \<Rightarrow> _ \<Rightarrow> 'v multiset\<close> where
  \<open>all_atms_init N NUE = atm_of `# all_lits N NUE\<close>

abbreviation all_atms_st_init :: \<open>'v twl_st_wl_init \<Rightarrow> 'v multiset\<close> where
  \<open>all_atms_st_init S \<equiv> atm_of `# all_lits_st_init S\<close>

lemma DECISION_REASON0[simp]: \<open>DECISION_REASON \<noteq> 0\<close>
  by (auto simp: DECISION_REASON_def)

lemma propagate_unit_cls_heur_propagate_unit_cls:
  \<open>(uncurry propagate_unit_cls_heur, uncurry (RETURN oo propagate_unit_init_wl)) \<in>
   [\<lambda>(L, S). undefined_lit (get_trail_init_wl S) L \<and> L \<in># \<L>\<^sub>a\<^sub>l\<^sub>l \<A>]\<^sub>f
    Id \<times>\<^sub>r twl_st_heur_parsing_no_WL \<A> unbdd \<rightarrow> \<langle>twl_st_heur_parsing_no_WL \<A> unbdd\<rangle> nres_rel\<close>
  unfolding  twl_st_heur_parsing_no_WL_def propagate_unit_cls_heur_def propagate_unit_init_wl_def
  apply (intro frefI nres_relI)
  apply (clarsimp simp add: propagate_unit_init_wl.simps cons_trail_Propagated_def[symmetric] comp_def
    curry_def all_atms_def[symmetric] intro!: ASSERT_leI)
  apply (rule ASSERT_leI)
  subgoal for aa ab ac ad ae b af ag ah ba ai aj ak al am an bb ao bc ap aq ar bd as be
       at au av aw ax ay bg
    by (rule cons_trail_Propagated_tr_pre[of _ _ \<A>])
      (auto simp: DECISION_REASON_def dest: \<L>\<^sub>a\<^sub>l\<^sub>l_cong)
  apply (rule RETURN_refine)
  apply (subst in_pair_collect_simp)
  apply (simp only: prod.simps)
  apply (intro conjI)
  subgoal by fast
  subgoal by (auto simp: all_lits_of_mm_add_mset all_lits_of_m_add_mset uminus_\<A>\<^sub>i\<^sub>n_iff)
  subgoal by (auto simp: all_lits_of_mm_add_mset all_lits_of_m_add_mset uminus_\<A>\<^sub>i\<^sub>n_iff)
  subgoal
    unfolding DECISION_REASON_def
    by (auto intro!: cons_trail_Propagated_tr[of \<A>, THEN fref_to_Down_unRET_uncurry2]
      dest: \<L>\<^sub>a\<^sub>l\<^sub>l_cong)
  subgoal by fast
  supply cons_trail_Propagated_def[simp]
  subgoal by auto
  subgoal by auto
  subgoal by (auto intro!: isa_vmtf_init_consD)
  subgoal by fast
  subgoal by auto
  subgoal by fast
  subgoal by (auto simp: all_lits_of_mm_add_mset all_lits_of_m_add_mset uminus_\<A>\<^sub>i\<^sub>n_iff)
  subgoal by auto
  done

definition already_propagated_unit_cls
   :: \<open>nat literal \<Rightarrow> nat twl_st_wl_init \<Rightarrow> nat twl_st_wl_init\<close>
where
  \<open>already_propagated_unit_cls = (\<lambda>L ((M, N, D, NE, UE, Q), OC).
     ((M, N, D, add_mset {#L#} NE, UE, Q), OC))\<close>

definition already_propagated_unit_cls_heur
   :: \<open>nat clause_l \<Rightarrow> twl_st_wl_heur_init \<Rightarrow> twl_st_wl_heur_init nres\<close>
where
  \<open>already_propagated_unit_cls_heur = (\<lambda>L (M, N, D, Q, oth).
     RETURN (M, N, D, Q, oth))\<close>

lemma already_propagated_unit_cls_heur_already_propagated_unit_cls:
  \<open>(uncurry already_propagated_unit_cls_heur, uncurry (RETURN oo already_propagated_unit_init_wl)) \<in>
  [\<lambda>(C, S). literals_are_in_\<L>\<^sub>i\<^sub>n \<A> C]\<^sub>f
  list_mset_rel \<times>\<^sub>r twl_st_heur_parsing_no_WL \<A> unbdd \<rightarrow> \<langle>twl_st_heur_parsing_no_WL \<A> unbdd\<rangle> nres_rel\<close>
  by (intro frefI nres_relI)
    (auto simp: twl_st_heur_parsing_no_WL_def already_propagated_unit_cls_heur_def
     already_propagated_unit_init_wl_def all_lits_of_mm_add_mset all_lits_of_m_add_mset
     literals_are_in_\<L>\<^sub>i\<^sub>n_def)

definition (in -) set_conflict_unit :: \<open>nat literal \<Rightarrow> nat clause option \<Rightarrow> nat clause option\<close> where
\<open>set_conflict_unit L _ = Some {#L#}\<close>

definition set_conflict_unit_heur where
  \<open>set_conflict_unit_heur = (\<lambda> L (b, n, xs). RETURN (False, 1, xs[atm_of L := Some (is_pos L)]))\<close>

lemma set_conflict_unit_heur_set_conflict_unit:
  \<open>(uncurry set_conflict_unit_heur, uncurry (RETURN oo set_conflict_unit)) \<in>
    [\<lambda>(L, D). D = None \<and> L \<in># \<L>\<^sub>a\<^sub>l\<^sub>l \<A>]\<^sub>f Id \<times>\<^sub>f option_lookup_clause_rel \<A> \<rightarrow>
     \<langle>option_lookup_clause_rel \<A>\<rangle>nres_rel\<close>
  by (intro frefI nres_relI)
    (auto simp: twl_st_heur_def set_conflict_unit_heur_def set_conflict_unit_def
      option_lookup_clause_rel_def lookup_clause_rel_def in_\<L>\<^sub>a\<^sub>l\<^sub>l_atm_of_in_atms_of_iff
      intro!: mset_as_position.intros)

definition conflict_propagated_unit_cls
 :: \<open>nat literal \<Rightarrow> nat twl_st_wl_init \<Rightarrow> nat twl_st_wl_init\<close>
where
  \<open>conflict_propagated_unit_cls = (\<lambda>L ((M, N, D, NE, UE, Q), OC).
     ((M, N, set_conflict_unit L D, add_mset {#L#} NE, UE, {#}), OC))\<close>

definition conflict_propagated_unit_cls_heur
  :: \<open>nat literal \<Rightarrow> twl_st_wl_heur_init \<Rightarrow> twl_st_wl_heur_init nres\<close>
where
  \<open>conflict_propagated_unit_cls_heur = (\<lambda>L (M, N, D, Q, oth). do {
     ASSERT(atm_of L < length (snd (snd D)));
     D \<leftarrow> set_conflict_unit_heur L D;
     ASSERT(isa_length_trail_pre M);
     RETURN (M, N, D, isa_length_trail M, oth)
    })\<close>

lemma conflict_propagated_unit_cls_heur_conflict_propagated_unit_cls:
  \<open>(uncurry conflict_propagated_unit_cls_heur, uncurry (RETURN oo set_conflict_init_wl)) \<in>
   [\<lambda>(L, S). L \<in># \<L>\<^sub>a\<^sub>l\<^sub>l \<A> \<and> get_conflict_init_wl S = None]\<^sub>f
      nat_lit_lit_rel \<times>\<^sub>r twl_st_heur_parsing_no_WL \<A> unbdd \<rightarrow> \<langle>twl_st_heur_parsing_no_WL \<A> unbdd\<rangle> nres_rel\<close>
proof -
  have set_conflict_init_wl_alt_def:
    \<open>RETURN oo set_conflict_init_wl = (\<lambda>L ((M, N, D, NE, UE, Q), OC). do {
      D \<leftarrow> RETURN (set_conflict_unit L D);
      RETURN ((M, N, Some {#L#}, add_mset {#L#} NE, UE, {#}), OC)
   })\<close>
    by (auto intro!: ext simp: set_conflict_init_wl_def)
  have [refine0]: \<open>D = None \<and> L \<in># \<L>\<^sub>a\<^sub>l\<^sub>l \<A> \<Longrightarrow> (y, None) \<in> option_lookup_clause_rel \<A> \<Longrightarrow> L = L' \<Longrightarrow>
    set_conflict_unit_heur L' y \<le> \<Down> {(D, D'). (D, D') \<in> option_lookup_clause_rel \<A> \<and> D' = Some {#L#}}
       (RETURN (set_conflict_unit L D))\<close>
    for L D y L'
    apply (rule order_trans)
    apply (rule set_conflict_unit_heur_set_conflict_unit[THEN fref_to_Down_curry,
      unfolded comp_def, of  \<A> L D L' y])
    subgoal
      by auto
    subgoal
      by auto
    subgoal
      unfolding conc_fun_RETURN
      by (auto simp: set_conflict_unit_def)
    done

  show ?thesis
    supply RETURN_as_SPEC_refine[refine2 del]
    unfolding set_conflict_init_wl_alt_def conflict_propagated_unit_cls_heur_def uncurry_def
    apply (intro frefI nres_relI)
    apply (refine_rcg)
    subgoal
      by (auto simp: twl_st_heur_parsing_no_WL_def option_lookup_clause_rel_def
        lookup_clause_rel_def atms_of_def)
    subgoal
      by auto
    subgoal
      by auto
    subgoal
      by (auto simp: twl_st_heur_parsing_no_WL_def conflict_propagated_unit_cls_heur_def conflict_propagated_unit_cls_def
        image_image set_conflict_unit_def
        intro!: set_conflict_unit_heur_set_conflict_unit[THEN fref_to_Down_curry])
    subgoal
      by auto
    subgoal
      by (auto simp: twl_st_heur_parsing_no_WL_def conflict_propagated_unit_cls_heur_def
          conflict_propagated_unit_cls_def
        intro!: isa_length_trail_pre)
    subgoal
      by (auto simp: twl_st_heur_parsing_no_WL_def conflict_propagated_unit_cls_heur_def
        conflict_propagated_unit_cls_def
        image_image set_conflict_unit_def all_lits_of_mm_add_mset all_lits_of_m_add_mset uminus_\<A>\<^sub>i\<^sub>n_iff
	isa_length_trail_length_u[THEN fref_to_Down_unRET_Id]
        intro!: set_conflict_unit_heur_set_conflict_unit[THEN fref_to_Down_curry]
	  isa_length_trail_pre)
    done
qed

definition add_init_cls_heur
  :: \<open>bool \<Rightarrow> nat clause_l \<Rightarrow> twl_st_wl_heur_init \<Rightarrow> twl_st_wl_heur_init nres\<close>  where
  \<open>add_init_cls_heur unbdd = (\<lambda>C (M, N, D, Q, W, vm, \<phi>, clvls, cach, lbd, vdom, failed). do {
     let C = C;
     ASSERT(length C \<le> uint_max + 2);
     ASSERT(length C \<ge> 2);
     if unbdd \<or> (length N \<le> uint64_max - length C - 5 \<and> \<not>failed)
     then do {
       ASSERT(length vdom \<le> length N);
       (N, i) \<leftarrow> fm_add_new True C N;
       RETURN (M, N, D, Q, W, vm, \<phi>, clvls, cach, lbd, vdom @ [nat_of_uint32_conv i], failed)
     } else RETURN (M, N, D, Q, W, vm, \<phi>, clvls, cach, lbd, vdom, True)})\<close>

definition add_init_cls_heur_unb :: \<open>nat clause_l \<Rightarrow> twl_st_wl_heur_init \<Rightarrow> twl_st_wl_heur_init nres\<close> where
\<open>add_init_cls_heur_unb = add_init_cls_heur True\<close>

definition add_init_cls_heur_b :: \<open>nat clause_l \<Rightarrow> twl_st_wl_heur_init \<Rightarrow> twl_st_wl_heur_init nres\<close> where
\<open>add_init_cls_heur_b = add_init_cls_heur False\<close>

lemma length_C_nempty_iff: \<open>length C \<ge> 2 \<longleftrightarrow> C \<noteq> [] \<and> tl C \<noteq> []\<close>
  by (cases C; cases \<open>tl C\<close>) auto

context
  fixes unbdd :: bool and \<A> :: \<open>nat multiset\<close> and
    x :: \<open>nat literal list \<times>
              (nat literal list \<times>
               bool option list \<times> nat list \<times> nat list \<times> nat \<times> nat list) \<times>
              arena_el list \<times>
              (bool \<times> nat \<times> bool option list) \<times>
              nat \<times>
              (nat \<times> nat literal \<times> bool) list list \<times>
              (((nat, nat) vmtf_node list \<times>
                nat \<times> nat option \<times> nat option \<times> nat option) \<times>
               nat list \<times> bool list) \<times>
              bool list \<times>
              nat \<times>
              (minimize_status list \<times> nat list) \<times>
              bool list \<times>
              nat list \<times> bool\<close> and y :: \<open>nat literal list \<times>
                                  ((nat literal, nat literal,
                                    nat) annotated_lit list \<times>
                                   (nat, nat literal list \<times> bool) fmap \<times>
                                   nat literal multiset option \<times>
                                   nat literal multiset multiset \<times>
                                   nat literal multiset multiset \<times>
                                   nat literal multiset) \<times>
                                  nat literal multiset multiset\<close> and x1 :: \<open>nat literal list\<close> and x2 :: \<open>((nat literal,
                           nat literal, nat) annotated_lit list \<times>
                          (nat, nat literal list \<times> bool) fmap \<times>
                          nat literal multiset option \<times>
                          nat literal multiset multiset \<times>
                          nat literal multiset multiset \<times>
                          nat literal multiset) \<times>
                         nat literal multiset multiset\<close> and x1a :: \<open>(nat literal,
                             nat literal, nat) annotated_lit list \<times>
                            (nat, nat literal list \<times> bool) fmap \<times>
                            nat literal multiset option \<times>
                            nat literal multiset multiset \<times>
                            nat literal multiset multiset \<times>
                            nat literal multiset\<close> and x1b :: \<open>(nat literal,
                       nat literal,
                       nat) annotated_lit list\<close> and x2a :: \<open>(nat,
                     nat literal list \<times> bool) fmap \<times>
                    nat literal multiset option \<times>
                    nat literal multiset multiset \<times>
                    nat literal multiset multiset \<times>
                    nat literal multiset\<close> and x1c :: \<open>(nat,
               nat literal list \<times>
               bool) fmap\<close> and x2b :: \<open>nat literal multiset option \<times>
                                       nat literal multiset multiset \<times>
                                       nat literal multiset multiset \<times>
                                       nat literal multiset\<close> and x1d :: \<open>nat literal multiset option\<close> and x2c :: \<open>nat literal multiset multiset \<times>
                                  nat literal multiset multiset \<times>
                                  nat literal multiset\<close> and x1e :: \<open>nat literal multiset multiset\<close> and x2d :: \<open>nat literal multiset multiset \<times>
                               nat literal multiset\<close> and x1f :: \<open>nat literal multiset multiset\<close> and x2e :: \<open>nat literal multiset\<close> and x2f :: \<open>nat literal multiset multiset\<close> and x1g :: \<open>nat literal list\<close> and x2g :: \<open>(nat literal list \<times>
                bool option list \<times> nat list \<times> nat list \<times> nat \<times> nat list) \<times>
               arena_el list \<times>
               (bool \<times> nat \<times> bool option list) \<times>
               nat \<times>
               (nat \<times> nat literal \<times> bool) list list \<times>
               (((nat, nat) vmtf_node list \<times>
                 nat \<times> nat option \<times> nat option \<times> nat option) \<times>
                nat list \<times> bool list) \<times>
               bool list \<times>
               nat \<times>
               (minimize_status list \<times> nat list) \<times>
               bool list \<times>
               nat list  \<times> bool\<close> and x1h :: \<open>nat literal list \<times>
                                     bool option list \<times>
                                     nat list \<times>
                                     nat list \<times>
                                     nat \<times>
                                     nat list\<close> and x2h :: \<open>arena_el list \<times>
                   (bool \<times> nat \<times> bool option list) \<times>
                   nat \<times>
                   (nat \<times> nat literal \<times> bool) list list \<times>
                   (((nat, nat) vmtf_node list \<times>
                     nat \<times> nat option \<times> nat option \<times> nat option) \<times>
                    nat list \<times> bool list) \<times>
                   bool list \<times>
                   nat \<times>
                   (minimize_status list \<times> nat list) \<times>
                   bool list \<times>
                   nat list \<times> bool\<close> and x1i :: \<open>arena_el list\<close> and x2i :: \<open>(bool \<times>
                             nat \<times> bool option list) \<times>
                            nat \<times>
                            (nat \<times> nat literal \<times> bool) list list \<times>
                            (((nat, nat) vmtf_node list \<times>
                              nat \<times> nat option \<times> nat option \<times> nat option) \<times>
                             nat list \<times> bool list) \<times>
                            bool list \<times>
                            nat \<times>
                            (minimize_status list \<times> nat list) \<times>
                            bool list \<times>
                            nat list \<times> bool\<close> and x1j :: \<open>bool \<times>
          nat \<times>
          bool option list\<close> and x2j :: \<open>nat \<times>
(nat \<times> nat literal \<times> bool) list list \<times>
(((nat, nat) vmtf_node list \<times> nat \<times> nat option \<times> nat option \<times> nat option) \<times>
 nat list \<times> bool list) \<times>
bool list \<times>
nat \<times>
(minimize_status list \<times> nat list) \<times>
bool list \<times>
nat list \<times> bool\<close> and x1k :: \<open>nat\<close> and x2k :: \<open>(nat \<times> nat literal \<times> bool) list list \<times>
                                       (((nat, nat) vmtf_node list \<times>
 nat \<times> nat option \<times> nat option \<times> nat option) \<times>
nat list \<times> bool list) \<times>
                                       bool list \<times>
                                       nat \<times>
                                       (minimize_status list \<times> nat list) \<times>
                                       bool list \<times>
                                       nat list \<times> bool\<close> and x1l :: \<open>(nat \<times>
                      nat literal \<times>
                      bool) list list\<close> and x2l :: \<open>(((nat, nat) vmtf_node list \<times>
             nat \<times> nat option \<times> nat option \<times> nat option) \<times>
            nat list \<times> bool list) \<times>
           bool list \<times>
           nat \<times>
           (minimize_status list \<times> nat list) \<times>
           bool list \<times>
           nat list \<times> _\<close> and x1m :: \<open>((nat, nat) vmtf_node list \<times>
                                  nat \<times> nat option \<times> nat option \<times> nat option) \<times>
                                 nat list \<times>
                                 bool list\<close> and x2m :: \<open>bool list \<times>
                nat \<times>
                (minimize_status list \<times> nat list) \<times>
                bool list \<times>
                nat list \<times> bool\<close> and x1n :: \<open>bool list\<close> and x2n :: \<open>nat \<times>
                     (minimize_status list \<times> nat list) \<times>
                     bool list \<times>
                     nat list \<times> bool\<close> and x1o :: \<open>nat\<close> and x2o :: \<open>(minimize_status list \<times>
                     nat list) \<times>
                    bool list \<times>
                    nat list \<times> bool\<close> and x1p :: \<open>minimize_status list \<times>
  nat list\<close> and x2p :: \<open>bool list \<times>
                        nat list \<times> bool\<close> and x1q :: \<open>bool list\<close> and x2q :: \<open>nat list  \<times> bool\<close> and x1r' :: \<open>nat list\<close> and x2r' :: bool
  assumes
    pre: \<open>case y of
     (C, S) \<Rightarrow> 2 \<le> length C \<and> literals_are_in_\<L>\<^sub>i\<^sub>n \<A> (mset C) \<and> distinct C\<close> and
    xy: \<open>(x, y) \<in> Id \<times>\<^sub>f twl_st_heur_parsing_no_WL \<A> unbdd\<close> and
    st:
      \<open>x2d = (x1f, x2e)\<close>
      \<open>x2c = (x1e, x2d)\<close>
      \<open>x2b = (x1d, x2c)\<close>
      \<open>x2a = (x1c, x2b)\<close>
      \<open>x1a = (x1b, x2a)\<close>
      \<open>x2 = (x1a, x2f)\<close>
      \<open>y = (x1, x2)\<close>
      \<open>x2q = (x1r', x2r')\<close>
      \<open>x2p = (x1q, x2q)\<close>
      \<open>x2o = (x1p, x2p)\<close>
      \<open>x2n = (x1o, x2o)\<close>
      \<open>x2m = (x1n, x2n)\<close>
      \<open>x2l = (x1m, x2m)\<close>
      \<open>x2k = (x1l, x2l)\<close>
      \<open>x2j = (x1k, x2k)\<close>
      \<open>x2i = (x1j, x2j)\<close>
      \<open>x2h = (x1i, x2i)\<close>
      \<open>x2g = (x1h, x2h)\<close>
      \<open>x = (x1g, x2g)\<close>
begin

lemma add_init_pre1: \<open>length x1g \<le> uint_max + 2\<close>
  using pre clss_size_uint_max[of \<A> \<open>mset x1g\<close>] xy st
  by (auto simp: twl_st_heur_parsing_no_WL_def)

lemma add_init_pre2: \<open>2 \<le> length x1g\<close>
  using pre xy st by (auto simp: )

private lemma
    x1g_x1: \<open>x1g = x1\<close> and
    \<open>(x1h, x1b) \<in> trail_pol \<A>\<close> and
   valid: \<open> valid_arena x1i x1c (set x1r')\<close> and
    \<open>(x1j, x1d) \<in> option_lookup_clause_rel \<A>\<close> and    \<open>x1k \<le> length x1b\<close> and
    \<open>x2e = {#- lit_of x. x \<in># mset (drop x1k (rev x1b))#}\<close> and
    \<open>x1m \<in> isa_vmtf_init \<A> x1b\<close> and
    \<open>phase_saving \<A> x1n\<close> and
    \<open>no_dup x1b\<close> and
    \<open>cach_refinement_empty \<A> x1p\<close> and
    vdom: \<open>mset x1r' = dom_m x1c\<close> and
    var_incl:
     \<open>set_mset (all_lits_of_mm ({#mset (fst x). x \<in># ran_m x1c#} + x1e + x1f))
       \<subseteq> set_mset (\<L>\<^sub>a\<^sub>l\<^sub>l \<A>)\<close> and
    watched: \<open>(x1l, empty_watched \<A>) \<in> \<langle>Id\<rangle>map_fun_rel (D\<^sub>0 \<A>)\<close> and
    bounded: \<open>isasat_input_bounded \<A>\<close>
    if \<open>\<not>x2r'  \<or> unbdd\<close>
  using that xy unfolding st twl_st_heur_parsing_no_WL_def
  by auto

lemma init_fm_add_new:
  \<open>\<not>x2r'  \<or> unbdd \<Longrightarrow> fm_add_new True x1g x1i
       \<le> \<Down>  {((arena, i), (N', i')). valid_arena arena N' (insert i (set x1r')) \<and> i = i' \<and>
              i \<notin># dom_m x1c \<and> i = length x1i + header_size x1g \<and>
	      i \<notin> set x1r'}
          (SPEC
            (\<lambda>(N', ia).
                0 < ia \<and> ia \<notin># dom_m x1c \<and> N' = fmupd ia (x1, True) x1c))\<close>
  (is \<open>_ \<Longrightarrow> _ \<le> \<Down> ?qq _\<close>)
  unfolding x1g_x1
  apply (rule order_trans)
  apply (rule fm_add_new_append_clause)
  using valid vdom pre xy valid_arena_in_vdom_le_arena[OF valid] arena_lifting(2)[OF valid]
    valid unfolding st
  by (fastforce simp: x1g_x1 vdom_m_def
    intro!: RETURN_RES_refine valid_arena_append_clause)


lemma add_init_cls_final_rel:
  fixes xa :: \<open>arena_el list \<times>
               nat\<close> and x' :: \<open>(nat, nat literal list \<times> bool) fmap \<times>
                               nat\<close> and x1r :: \<open>(nat,
         nat literal list \<times>
         bool) fmap\<close> and x2r :: \<open>nat\<close> and x1s :: \<open>arena_el list\<close> and x2s :: \<open>nat\<close>
  assumes
    \<open>(xa, x')
     \<in> {((arena, i), (N', i')). valid_arena arena N' (insert i (set x1r')) \<and> i = i' \<and>
              i \<notin># dom_m x1c \<and> i = length x1i + header_size x1g \<and>
              i \<notin> set x1r'}\<close> and
    \<open>x' \<in> {(N', ia).
           0 < ia \<and> ia \<notin># dom_m x1c \<and> N' = fmupd ia (x1, True) x1c}\<close> and
    \<open>x' = (x1r, x2r)\<close> and
    \<open>xa = (x1s, x2s)\<close>
  shows \<open>((x1h, x1s, x1j, x1k, x1l, x1m, x1n, x1o, x1p, x1q,
           x1r' @ [nat_of_uint32_conv x2s], x2r'),
          (x1b, x1r, x1d, x1e, x1f, x2e), x2f)
         \<in> twl_st_heur_parsing_no_WL \<A> unbdd\<close>
proof -
  show ?thesis
  using assms xy pre unfolding st
    apply (auto simp: twl_st_heur_parsing_no_WL_def nat_of_uint32_conv_def
      intro!: )
    apply (auto simp: vdom_m_simps5 ran_m_mapsto_upd_notin all_lits_of_mm_add_mset
      literals_are_in_\<L>\<^sub>i\<^sub>n_def)
    done
qed
end


lemma add_init_cls_heur_add_init_cls:
  \<open>(uncurry (add_init_cls_heur unbdd), uncurry (add_to_clauses_init_wl)) \<in>
   [\<lambda>(C, S). length C \<ge> 2 \<and> literals_are_in_\<L>\<^sub>i\<^sub>n \<A> (mset C) \<and> distinct C]\<^sub>f
   Id \<times>\<^sub>r twl_st_heur_parsing_no_WL \<A> unbdd  \<rightarrow> \<langle>twl_st_heur_parsing_no_WL \<A> unbdd\<rangle> nres_rel\<close>
proof -
  have \<open>42 + Max_mset (add_mset 0 (x1c)) \<notin># x1c\<close> and \<open>42 + Max_mset (add_mset (0 :: nat) (x1c)) \<noteq> 0\<close> for x1c
    apply  (cases \<open>x1c\<close>) apply (auto simp: max_def)
    apply (metis Max_ge add.commute add.right_neutral add_le_cancel_left finite_set_mset le_zero_eq set_mset_add_mset_insert union_single_eq_member zero_neq_numeral)
    by (smt Max_ge Set.set_insert add.commute add.right_neutral add_mset_commute antisym diff_add_inverse diff_le_self finite_insert finite_set_mset insert_DiffM insert_commute set_mset_add_mset_insert union_single_eq_member zero_neq_numeral)
  then have [iff]: \<open>(\<forall>b. b = (0::nat) \<or> b \<in># x1c) \<longleftrightarrow> False\<close> \<open>\<exists>b>0. b \<notin># x1c\<close>for x1c
    by blast+
  have add_to_clauses_init_wl_alt_def:
   \<open>add_to_clauses_init_wl = (\<lambda>i ((M, N, D, NE, UE, Q), OC). do {
     let b = (length i = 2);
    (N', ia) \<leftarrow> SPEC (\<lambda>(N', ia). ia > 0 \<and> ia \<notin># dom_m N \<and> N' = fmupd ia (i, True) N);
    RETURN ((M, N', D, NE, UE, Q), OC)
  })\<close>
    by (auto simp: add_to_clauses_init_wl_def get_fresh_index_def Let_def
     RES_RES2_RETURN_RES RES_RES_RETURN_RES2 RES_RETURN_RES uncurry_def image_iff
    intro!: ext)
  show ?thesis
    unfolding add_init_cls_heur_def add_to_clauses_init_wl_alt_def uncurry_def Let_def
      to_watcher_def id_def
    apply (intro frefI nres_relI)
    apply (refine_vcg init_fm_add_new)
    subgoal
      by (rule add_init_pre1)
    subgoal
      by (rule add_init_pre2)
    apply (rule lhs_step_If)
    apply (refine_rcg)
    subgoal unfolding twl_st_heur_parsing_no_WL_def
        by (force dest!: valid_arena_vdom_le(2) simp: distinct_card)
    apply (rule init_fm_add_new)
    apply assumption+
    subgoal by auto
    subgoal by (rule add_init_cls_final_rel)
    unfolding RES_RES2_RETURN_RES RETURN_def
      apply simp
    subgoal unfolding RETURN_def apply (rule RES_refine)
      by (auto simp: twl_st_heur_parsing_no_WL_def RETURN_def intro!: RES_refine)
    done
qed

definition already_propagated_unit_cls_conflict
  :: \<open>nat literal \<Rightarrow> nat twl_st_wl_init \<Rightarrow> nat twl_st_wl_init\<close>
where
  \<open>already_propagated_unit_cls_conflict = (\<lambda>L ((M, N, D, NE, UE, Q), OC).
     ((M, N, D, add_mset {#L#} NE, UE, {#}), OC))\<close>

definition already_propagated_unit_cls_conflict_heur
  :: \<open>nat literal \<Rightarrow> twl_st_wl_heur_init \<Rightarrow> twl_st_wl_heur_init nres\<close>
where
  \<open>already_propagated_unit_cls_conflict_heur = (\<lambda>L (M, N, D, Q, oth). do {
     ASSERT (isa_length_trail_pre M);
     RETURN (M, N, D, isa_length_trail M, oth)
  })\<close>

lemma already_propagated_unit_cls_conflict_heur_already_propagated_unit_cls_conflict:
  \<open>(uncurry already_propagated_unit_cls_conflict_heur,
     uncurry (RETURN oo already_propagated_unit_cls_conflict)) \<in>
   [\<lambda>(L, S). L \<in># \<L>\<^sub>a\<^sub>l\<^sub>l \<A>]\<^sub>f Id \<times>\<^sub>r twl_st_heur_parsing_no_WL \<A> unbdd \<rightarrow> \<langle>twl_st_heur_parsing_no_WL \<A> unbdd\<rangle> nres_rel\<close>
  by (intro frefI nres_relI)
    (auto simp: twl_st_heur_parsing_no_WL_def already_propagated_unit_cls_conflict_heur_def
      already_propagated_unit_cls_conflict_def all_lits_of_mm_add_mset
      all_lits_of_m_add_mset uminus_\<A>\<^sub>i\<^sub>n_iff isa_length_trail_length_u[THEN fref_to_Down_unRET_Id]
      intro: vmtf_consD
      intro!: ASSERT_leI isa_length_trail_pre)

definition (in -) set_conflict_empty :: \<open>nat clause option \<Rightarrow> nat clause option\<close> where
\<open>set_conflict_empty _ = Some {#}\<close>

definition (in -) lookup_set_conflict_empty :: \<open>conflict_option_rel \<Rightarrow> conflict_option_rel\<close> where
\<open>lookup_set_conflict_empty  = (\<lambda>(b, s) . (False, s))\<close>

lemma lookup_set_conflict_empty_set_conflict_empty:
  \<open>(RETURN o lookup_set_conflict_empty, RETURN o set_conflict_empty) \<in>
     [\<lambda>D. D = None]\<^sub>f option_lookup_clause_rel \<A> \<rightarrow> \<langle>option_lookup_clause_rel \<A>\<rangle>nres_rel\<close>
  by (intro frefI nres_relI) (auto simp: set_conflict_empty_def
      lookup_set_conflict_empty_def option_lookup_clause_rel_def
      lookup_clause_rel_def)


definition set_empty_clause_as_conflict_heur
   :: \<open>twl_st_wl_heur_init \<Rightarrow> twl_st_wl_heur_init nres\<close> where
  \<open>set_empty_clause_as_conflict_heur = (\<lambda> (M, N, (_, (n, xs)), Q, WS). do {
     ASSERT(isa_length_trail_pre M);
     RETURN (M, N, (False, (n, xs)), isa_length_trail M, WS)})\<close>

lemma set_empty_clause_as_conflict_heur_set_empty_clause_as_conflict:
  \<open>(set_empty_clause_as_conflict_heur, RETURN o add_empty_conflict_init_wl) \<in>
  [\<lambda>S. get_conflict_init_wl S = None]\<^sub>f
   twl_st_heur_parsing_no_WL \<A> unbdd \<rightarrow> \<langle>twl_st_heur_parsing_no_WL \<A> unbdd\<rangle> nres_rel\<close>
  by (intro frefI nres_relI)
    (auto simp: set_empty_clause_as_conflict_heur_def add_empty_conflict_init_wl_def
      twl_st_heur_parsing_no_WL_def set_conflict_empty_def option_lookup_clause_rel_def
      lookup_clause_rel_def isa_length_trail_length_u[THEN fref_to_Down_unRET_Id]
       intro!: isa_length_trail_pre ASSERT_leI)


definition (in -) add_clause_to_others_heur
   :: \<open>nat clause_l \<Rightarrow> twl_st_wl_heur_init \<Rightarrow> twl_st_wl_heur_init nres\<close> where
  \<open>add_clause_to_others_heur = (\<lambda> _ (M, N, D, Q, WS).
      RETURN (M, N, D, Q, WS))\<close>

lemma add_clause_to_others_heur_add_clause_to_others:
  \<open>(uncurry add_clause_to_others_heur, uncurry (RETURN oo add_to_other_init)) \<in>
   \<langle>Id\<rangle>list_rel \<times>\<^sub>r twl_st_heur_parsing_no_WL \<A> unbdd \<rightarrow>\<^sub>f \<langle>twl_st_heur_parsing_no_WL \<A> unbdd\<rangle> nres_rel\<close>
  by (intro frefI nres_relI)
    (auto simp: add_clause_to_others_heur_def add_to_other_init.simps
      twl_st_heur_parsing_no_WL_def)


definition (in -)list_length_1 where
  [simp]: \<open>list_length_1 C \<longleftrightarrow> length C = 1\<close>

definition (in -)list_length_1_code where
  \<open>list_length_1_code C \<longleftrightarrow> (case C of [_] \<Rightarrow> True | _ \<Rightarrow> False)\<close>


definition (in -) get_conflict_wl_is_None_heur_init :: \<open>twl_st_wl_heur_init \<Rightarrow> bool\<close> where
  \<open>get_conflict_wl_is_None_heur_init = (\<lambda>(M, N, (b, _), Q, _). b)\<close>


definition init_dt_step_wl_heur
  :: \<open>bool \<Rightarrow> nat clause_l \<Rightarrow> twl_st_wl_heur_init \<Rightarrow> (twl_st_wl_heur_init) nres\<close>
where
  \<open>init_dt_step_wl_heur unbdd C S = do {
     if get_conflict_wl_is_None_heur_init S
     then do {
        if is_Nil C
        then set_empty_clause_as_conflict_heur S
        else if list_length_1 C
        then do {
          ASSERT (C \<noteq> []);
          let L = hd C;
          ASSERT(polarity_pol_pre (get_trail_wl_heur_init S) L);
          let val_L = polarity_pol (get_trail_wl_heur_init S) L;
          if val_L = None
          then propagate_unit_cls_heur L S
          else
            if val_L = Some True
            then already_propagated_unit_cls_heur C S
            else conflict_propagated_unit_cls_heur L S
        }
        else do {
          ASSERT(length C \<ge> 2);
          add_init_cls_heur unbdd C S
       }
     }
     else add_clause_to_others_heur C S
  }\<close>

named_theorems twl_st_heur_parsing_no_WL
lemma [twl_st_heur_parsing_no_WL]:
  assumes \<open>(S, T) \<in>  twl_st_heur_parsing_no_WL \<A> unbdd\<close>
  shows \<open>(get_trail_wl_heur_init S, get_trail_init_wl T) \<in> trail_pol \<A>\<close>
  using assms
  by (cases S; auto simp: twl_st_heur_parsing_no_WL_def; fail)+


definition get_conflict_wl_is_None_init :: \<open>nat twl_st_wl_init \<Rightarrow> bool\<close> where
  \<open>get_conflict_wl_is_None_init = (\<lambda>((M, N, D, NE, UE, Q), OC). is_None D)\<close>

lemma get_conflict_wl_is_None_init_alt_def:
  \<open>get_conflict_wl_is_None_init S \<longleftrightarrow> get_conflict_init_wl S = None\<close>
  by (cases S) (auto simp: get_conflict_wl_is_None_init_def split: option.splits)

lemma get_conflict_wl_is_None_heur_get_conflict_wl_is_None_init:
    \<open>(RETURN o get_conflict_wl_is_None_heur_init,  RETURN o get_conflict_wl_is_None_init) \<in>
    twl_st_heur_parsing_no_WL \<A> unbdd \<rightarrow>\<^sub>f \<langle>Id\<rangle>nres_rel\<close>
  apply (intro frefI nres_relI)
  apply (rename_tac x y, case_tac x, case_tac y)
  by (auto simp: twl_st_heur_parsing_no_WL_def get_conflict_wl_is_None_heur_init_def option_lookup_clause_rel_def
      get_conflict_wl_is_None_init_def split: option.splits)


definition (in -) get_conflict_wl_is_None_init' where
  \<open>get_conflict_wl_is_None_init' = get_conflict_wl_is_None\<close>

lemma init_dt_step_wl_heur_init_dt_step_wl:
  \<open>(uncurry (init_dt_step_wl_heur unbdd), uncurry init_dt_step_wl) \<in>
   [\<lambda>(C, S). literals_are_in_\<L>\<^sub>i\<^sub>n \<A> (mset C) \<and> distinct C]\<^sub>f
      Id \<times>\<^sub>f twl_st_heur_parsing_no_WL \<A> unbdd \<rightarrow> \<langle>twl_st_heur_parsing_no_WL \<A> unbdd\<rangle> nres_rel\<close>
  supply [[goals_limit=1]]
  unfolding init_dt_step_wl_heur_def init_dt_step_wl_def uncurry_def
    option.case_eq_if get_conflict_wl_is_None_init_alt_def[symmetric]
  supply RETURN_as_SPEC_refine[refine2 del]
  apply (intro frefI nres_relI)
  apply (refine_vcg
      set_empty_clause_as_conflict_heur_set_empty_clause_as_conflict[THEN fref_to_Down,
        unfolded comp_def]
      propagate_unit_cls_heur_propagate_unit_cls[THEN fref_to_Down_curry, unfolded comp_def]
      already_propagated_unit_cls_heur_already_propagated_unit_cls[THEN fref_to_Down_curry,
        unfolded comp_def]
      conflict_propagated_unit_cls_heur_conflict_propagated_unit_cls[THEN fref_to_Down_curry,
        unfolded comp_def]
      add_init_cls_heur_add_init_cls[THEN fref_to_Down_curry,
        unfolded comp_def]
      add_clause_to_others_heur_add_clause_to_others[THEN fref_to_Down_curry,
        unfolded comp_def])
  subgoal by (auto simp: get_conflict_wl_is_None_heur_get_conflict_wl_is_None_init[THEN fref_to_Down_unRET_Id])
  subgoal by (auto simp: twl_st_heur_parsing_no_WL_def is_Nil_def split: list.splits)
  subgoal by (simp add: get_conflict_wl_is_None_init_alt_def)
  subgoal by auto
  subgoal by simp
  subgoal by simp
  subgoal by (auto simp: literals_are_in_\<L>\<^sub>i\<^sub>n_add_mset
    twl_st_heur_parsing_no_WL_def intro!: polarity_pol_pre split: list.splits)
  subgoal for C'S CT C T C' S
    by (subst polarity_pol_polarity[of \<A>, unfolded option_rel_id_simp,
       THEN fref_to_Down_unRET_uncurry_Id,
       of \<open>get_trail_init_wl T\<close> \<open>hd C\<close>])
      (auto simp: polarity_def twl_st_heur_parsing_no_WL_def
       polarity_pol_polarity[of \<A>, unfolded option_rel_id_simp, THEN fref_to_Down_unRET_uncurry_Id]
       literals_are_in_\<L>\<^sub>i\<^sub>n_add_mset
      split: list.splits)
  subgoal by (auto simp: twl_st_heur_parsing_no_WL_def)
  subgoal by (auto simp: twl_st_heur_parsing_no_WL_def   literals_are_in_\<L>\<^sub>i\<^sub>n_add_mset
      split: list.splits)
  subgoal by (auto simp: twl_st_heur_parsing_no_WL_def)
  subgoal for C'S CT C T C' S
    by (subst polarity_pol_polarity[of \<A>, unfolded option_rel_id_simp,
       THEN fref_to_Down_unRET_uncurry_Id,
       of \<open>get_trail_init_wl T\<close> \<open>hd C\<close>])
      (auto simp: polarity_def twl_st_heur_parsing_no_WL_def
       polarity_pol_polarity[of \<A>, unfolded option_rel_id_simp, THEN fref_to_Down_unRET_uncurry_Id]
       literals_are_in_\<L>\<^sub>i\<^sub>n_add_mset
      split: list.splits)
  subgoal by simp
  subgoal by (auto simp: list_mset_rel_def br_def)
  subgoal by (simp add:  literals_are_in_\<L>\<^sub>i\<^sub>n_add_mset
      split: list.splits)
  subgoal by (simp add: get_conflict_wl_is_None_init_alt_def)
  subgoal by simp
  subgoal
    by (auto simp: twl_st_heur_parsing_no_WL_def map_fun_rel_def literals_are_in_\<L>\<^sub>i\<^sub>n_add_mset
        split: list.splits)
  subgoal by simp
  subgoal
    by (auto simp: twl_st_heur_parsing_no_WL_def map_fun_rel_def literals_are_in_\<L>\<^sub>i\<^sub>n_add_mset
      split: list.splits)
  subgoal for x y x1 x2 C x2a
    by (cases C; cases \<open>tl C\<close>)
      (auto simp: twl_st_heur_parsing_no_WL_def map_fun_rel_def literals_are_in_\<L>\<^sub>i\<^sub>n_add_mset
        split: list.splits)
  subgoal by simp
  subgoal by simp
  subgoal by simp
  done

lemma (in -) get_conflict_wl_is_None_heur_init_alt_def:
  \<open>RETURN o get_conflict_wl_is_None_heur_init = (\<lambda>(M, N, (b, _), Q, W, _). RETURN b)\<close>
  by (auto simp: get_conflict_wl_is_None_heur_init_def intro!: ext)

definition polarity_st_heur_init :: \<open>twl_st_wl_heur_init \<Rightarrow> _ \<Rightarrow> bool option\<close> where
  \<open>polarity_st_heur_init = (\<lambda>(M, _) L. polarity_pol M L)\<close>

lemma polarity_st_heur_init_alt_def:
  \<open>polarity_st_heur_init S L = polarity_pol (get_trail_wl_heur_init S) L\<close>
  by (cases S) (auto simp: polarity_st_heur_init_def)


definition polarity_st_init :: \<open>'v twl_st_wl_init \<Rightarrow> 'v literal \<Rightarrow> bool option\<close> where
  \<open>polarity_st_init S = polarity (get_trail_init_wl S)\<close>

lemma get_conflict_wl_is_None_init:
   \<open>get_conflict_init_wl S = None \<longleftrightarrow> get_conflict_wl_is_None_init S\<close>
  by (cases S) (auto simp: get_conflict_wl_is_None_init_def split: option.splits)

definition init_dt_wl_heur
 :: \<open>bool \<Rightarrow> nat clause_l list \<Rightarrow> twl_st_wl_heur_init \<Rightarrow> twl_st_wl_heur_init nres\<close>
where
  \<open>init_dt_wl_heur unbdd CS S = nfoldli CS (\<lambda>_. True)
     (\<lambda>C S. do {
        init_dt_step_wl_heur unbdd C S}) S\<close>

definition init_dt_step_wl_heur_unb :: \<open>nat clause_l \<Rightarrow> twl_st_wl_heur_init \<Rightarrow> (twl_st_wl_heur_init) nres\<close> where
\<open>init_dt_step_wl_heur_unb = init_dt_step_wl_heur True\<close>

definition init_dt_wl_heur_unb :: \<open>nat clause_l list \<Rightarrow> twl_st_wl_heur_init \<Rightarrow> twl_st_wl_heur_init nres\<close> where
\<open>init_dt_wl_heur_unb = init_dt_wl_heur True\<close>

definition init_dt_step_wl_heur_b :: \<open>nat clause_l \<Rightarrow> twl_st_wl_heur_init \<Rightarrow> (twl_st_wl_heur_init) nres\<close> where
\<open>init_dt_step_wl_heur_b = init_dt_step_wl_heur False\<close>

definition init_dt_wl_heur_b :: \<open>nat clause_l list \<Rightarrow> twl_st_wl_heur_init \<Rightarrow> twl_st_wl_heur_init nres\<close> where
\<open>init_dt_wl_heur_b = init_dt_wl_heur False\<close>


subsection \<open>Extractions of the atoms in the state\<close>

definition init_valid_rep :: "nat list \<Rightarrow> nat set \<Rightarrow> bool" where
  \<open>init_valid_rep xs l \<longleftrightarrow>
      (\<forall>L\<in>l. L < length xs) \<and>
      (\<forall>L \<in> l.  (xs ! L) mod 2 = 1) \<and>
      (\<forall>L. L < length xs \<longrightarrow> (xs ! L) mod 2 = 1 \<longrightarrow> L \<in> l)\<close>

definition isasat_atms_ext_rel :: \<open>((nat list \<times> nat \<times> nat list) \<times> nat set) set\<close> where
  \<open>isasat_atms_ext_rel = {((xs, n, atms), l).
      init_valid_rep xs l \<and>
      n = Max (insert 0 l) \<and>
      length xs < uint_max \<and>
      (\<forall>s\<in>set xs. s \<le> uint64_max) \<and>
      finite l \<and>
      distinct atms \<and>
      set atms = l \<and>
      length xs \<noteq> 0
   }\<close>


lemma distinct_length_le_Suc_Max:
   assumes \<open>distinct (b :: nat list)\<close>
  shows \<open>length b \<le> Suc (Max (insert 0 (set b)))\<close>
proof -
  have \<open>set b \<subseteq> {0 ..< Suc (Max (insert 0 (set b)))}\<close>
    by (cases \<open>set b = {}\<close>)
     (auto simp add: le_imp_less_Suc)
  from card_mono[OF _ this] show ?thesis
     using distinct_card[OF assms(1)] by auto
qed

lemma isasat_atms_ext_rel_alt_def:
  \<open>isasat_atms_ext_rel = {((xs, n, atms), l).
      init_valid_rep xs l \<and>
      n = Max (insert 0 l) \<and>
      length xs < uint_max \<and>
      (\<forall>s\<in>set xs. s \<le> uint64_max) \<and>
      finite l \<and>
      distinct atms \<and>
      set atms = l \<and>
      length xs \<noteq> 0 \<and>
      length atms \<le> Suc n
   }\<close>
  by (auto simp: isasat_atms_ext_rel_def distinct_length_le_Suc_Max)


definition in_map_atm_of :: \<open>'a \<Rightarrow> 'a list \<Rightarrow> bool\<close> where
  \<open>in_map_atm_of L N \<longleftrightarrow> L \<in> set N\<close>

definition (in -) init_next_size where
  \<open>init_next_size L = 2 * L\<close>

lemma init_next_size: \<open>L \<noteq> 0 \<Longrightarrow> L + 1 \<le> uint_max \<Longrightarrow> L < init_next_size L\<close>
  by (auto simp: init_next_size_def uint32_max_uint32_def uint_max_def)

definition add_to_atms_ext where
  \<open>add_to_atms_ext = (\<lambda>i (xs, n, atms). do {
    ASSERT(i \<le> uint_max div 2);
    ASSERT(length xs \<le> uint_max);
    ASSERT(length atms \<le> Suc n);
    let n = max i n;
    (if i < length_uint32_nat xs then do {
       ASSERT(xs!i \<le> uint64_max);
       let atms = (if xs!i AND one_uint64_nat = one_uint64_nat then atms else atms @ [i]);
       RETURN (xs[i := (sum_mod_uint64_max (xs ! i) 2) OR one_uint64_nat], n, atms)
     }
     else do {
        ASSERT(i + 1 \<le> uint_max);
        ASSERT(length_uint32_nat xs \<noteq> 0);
        ASSERT(i < init_next_size i);
        RETURN ((list_grow xs (init_next_size i) zero_uint64_nat)[i := one_uint64_nat], n,
            atms @ [i])
     })
    })\<close>

lemma init_valid_rep_upd_OR:
  \<open>init_valid_rep (x1b[x1a := a OR one_uint64_nat]) x2 \<longleftrightarrow>
    init_valid_rep (x1b[x1a := one_uint64_nat]) x2 \<close> (is \<open>?A \<longleftrightarrow> ?B\<close>)
proof
  assume ?A
  then have
    1: \<open>\<forall>L\<in>x2. L < length (x1b[x1a := a OR one_uint64_nat])\<close> and
    2: \<open>\<forall>L\<in>x2. x1b[x1a := a OR one_uint64_nat] ! L mod 2 = 1\<close> and
    3: \<open>\<forall>L<length (x1b[x1a := a OR one_uint64_nat]).
        x1b[x1a := a OR one_uint64_nat] ! L mod 2 = 1 \<longrightarrow>
        L \<in> x2\<close>
    unfolding init_valid_rep_def by fast+
  have 1: \<open>\<forall>L\<in>x2. L < length (x1b[x1a := one_uint64_nat])\<close>
    using 1 by simp
  then have 2: \<open>\<forall>L\<in>x2. x1b[x1a := one_uint64_nat] ! L mod 2 = 1\<close>
    using 2 by (auto simp: nth_list_update')
  then have 3: \<open>\<forall>L<length (x1b[x1a := one_uint64_nat]).
        x1b[x1a := one_uint64_nat] ! L mod 2 = 1 \<longrightarrow>
        L \<in> x2\<close>
    using 3 by (auto split: if_splits simp: bitOR_1_if_mod_2_nat)
  show ?B
    using 1 2 3
    unfolding init_valid_rep_def by fast+
next
  assume ?B
  then have
    1: \<open>\<forall>L\<in>x2. L < length (x1b[x1a := one_uint64_nat])\<close> and
    2: \<open>\<forall>L\<in>x2. x1b[x1a := one_uint64_nat] ! L mod 2 = 1\<close> and
    3: \<open>\<forall>L<length (x1b[x1a := one_uint64_nat]).
        x1b[x1a := one_uint64_nat] ! L mod 2 = 1 \<longrightarrow>
        L \<in> x2\<close>
    unfolding init_valid_rep_def by fast+
  have 1: \<open>\<forall>L\<in>x2. L < length (x1b[x1a :=  a OR one_uint64_nat])\<close>
    using 1 by simp
  then have 2: \<open>\<forall>L\<in>x2. x1b[x1a := a OR one_uint64_nat] ! L mod 2 = 1\<close>
    using 2 by (auto simp: nth_list_update' bitOR_1_if_mod_2_nat)
  then have 3: \<open>\<forall>L<length (x1b[x1a :=  a OR one_uint64_nat]).
        x1b[x1a :=  a OR one_uint64_nat] ! L mod 2 = 1 \<longrightarrow>
        L \<in> x2\<close>
    using 3 by (auto split: if_splits simp: bitOR_1_if_mod_2_nat)
  show ?A
    using 1 2 3
    unfolding init_valid_rep_def by fast+
qed

lemma init_valid_rep_insert:
  assumes val: \<open>init_valid_rep x1b x2\<close> and le: \<open>x1a < length x1b\<close>
  shows \<open>init_valid_rep (x1b[x1a := one_uint64_nat]) (insert x1a x2)\<close>
proof -
  have
    1: \<open>\<forall>L\<in>x2. L < length x1b\<close> and
    2: \<open>\<forall>L\<in>x2. x1b ! L mod 2 = 1\<close> and
    3: \<open>\<And>L. L<length x1b \<Longrightarrow> x1b ! L mod 2 = 1 \<longrightarrow> L \<in> x2\<close>
    using val unfolding init_valid_rep_def by fast+
  have 1: \<open>\<forall>L\<in>insert x1a x2. L < length (x1b[x1a := one_uint64_nat])\<close>
    using 1 le by simp
  then have 2: \<open>\<forall>L\<in>insert x1a x2. x1b[x1a := one_uint64_nat] ! L mod 2 = 1\<close>
    using 2 by (auto simp: nth_list_update')
  then have 3: \<open>\<forall>L<length (x1b[x1a := one_uint64_nat]).
        x1b[x1a := one_uint64_nat] ! L mod 2 = 1 \<longrightarrow>
        L \<in> insert x1a x2\<close>
    using 3 le by (auto split: if_splits simp: bitOR_1_if_mod_2_nat)
  show ?thesis
    using 1 2 3
    unfolding init_valid_rep_def by fast+
qed

lemma init_valid_rep_extend:
  \<open>init_valid_rep (x1b @ replicate n 0) x2 \<longleftrightarrow> init_valid_rep (x1b) x2\<close>
   (is \<open>?A \<longleftrightarrow> ?B\<close> is \<open>init_valid_rep ?x1b _ \<longleftrightarrow> _\<close>)
proof
  assume ?A
  then have
    1: \<open>\<And>L. L\<in>x2 \<Longrightarrow> L < length ?x1b\<close> and
    2: \<open>\<And>L. L\<in>x2 \<Longrightarrow> ?x1b ! L mod 2 = 1\<close> and
    3: \<open>\<And>L. L<length ?x1b \<Longrightarrow> ?x1b ! L mod 2 = 1 \<longrightarrow> L \<in> x2\<close>
    unfolding init_valid_rep_def by fast+
  have 1: \<open>L\<in>x2 \<Longrightarrow> L < length x1b\<close> for L
    using 3[of L] 2[of L] 1[of L]
    by (auto simp: nth_append split: if_splits)
  then have 2: \<open>\<forall>L\<in>x2. x1b ! L mod 2 = 1\<close>
    using 2 by (auto simp: nth_list_update')
  then have 3: \<open>\<forall>L<length x1b. x1b ! L mod 2 = 1 \<longrightarrow> L \<in> x2\<close>
    using 3 by (auto split: if_splits simp: bitOR_1_if_mod_2_nat)
  show ?B
    using 1 2 3
    unfolding init_valid_rep_def by fast
next
  assume ?B
  then have
    1: \<open>\<And>L. L\<in>x2 \<Longrightarrow> L < length x1b\<close> and
    2: \<open>\<And>L. L\<in>x2 \<Longrightarrow> x1b ! L mod 2 = 1\<close> and
    3: \<open>\<And>L. L<length x1b \<longrightarrow> x1b ! L mod 2 = 1 \<longrightarrow> L \<in> x2\<close>
    unfolding init_valid_rep_def by fast+
  have 10: \<open>\<forall>L\<in>x2. L < length ?x1b\<close>
    using 1 by fastforce
  then have 20: \<open>L\<in>x2 \<Longrightarrow> ?x1b ! L mod 2 = 1\<close> for L
    using 1[of L] 2[of L] 3[of L] by (auto simp: nth_list_update' bitOR_1_if_mod_2_nat nth_append)
  then have 30: \<open>L<length ?x1b \<Longrightarrow> ?x1b ! L mod 2 = 1 \<longrightarrow> L \<in> x2\<close>for L
    using 1[of L] 2[of L] 3[of L]
    by (auto split: if_splits simp: bitOR_1_if_mod_2_nat nth_append)
  show ?A
    using 10 20 30
    unfolding init_valid_rep_def by fast+
qed

lemma init_valid_rep_in_set_iff:
  \<open>init_valid_rep x1b x2  \<Longrightarrow> x \<in> x2 \<longleftrightarrow> (x < length x1b \<and> (x1b!x) mod 2 = 1)\<close>
  unfolding init_valid_rep_def
  by auto

lemma add_to_atms_ext_op_set_insert:
  \<open>(uncurry add_to_atms_ext, uncurry (RETURN oo Set.insert))
   \<in> [\<lambda>(n, l). n \<le> uint_max div 2]\<^sub>f nat_rel \<times>\<^sub>f isasat_atms_ext_rel \<rightarrow> \<langle>isasat_atms_ext_rel\<rangle>nres_rel\<close>
proof -
  have H: \<open>finite x2 \<Longrightarrow> Max (insert x1 (insert 0 x2)) = Max (insert x1 x2)\<close>
    \<open>finite x2 \<Longrightarrow> Max (insert 0 (insert x1 x2)) = Max (insert x1 x2)\<close>
    for x1 and x2 :: \<open>nat set\<close>
    by (subst insert_commute) auto
  have [simp]: \<open>(a OR Suc 0) mod 2 = Suc 0\<close> for a
    by (auto simp add: bitOR_1_if_mod_2_nat)
  show ?thesis
    apply (intro frefI nres_relI)
    unfolding isasat_atms_ext_rel_def add_to_atms_ext_def uncurry_def
    apply (refine_vcg lhs_step_If)
    subgoal by auto
    subgoal by auto
    subgoal unfolding isasat_atms_ext_rel_def[symmetric] isasat_atms_ext_rel_alt_def by auto
    subgoal by auto
    subgoal for x y x1 x2 x1a x2a x1b x2b
      unfolding comp_def
      apply (rule RETURN_refine)
      apply (subst in_pair_collect_simp)
      apply (subst prod.case)+
      apply (intro conjI impI allI)
      subgoal by (simp add: init_valid_rep_upd_OR init_valid_rep_insert
            del: one_uint64_nat_def)
      subgoal by (auto simp: H Max_insert[symmetric] simp del: Max_insert)
      subgoal by auto
      subgoal
        using sum_mod_uint64_max_le_uint64_max
        unfolding bitOR_1_if_mod_2_nat one_uint64_nat_def
        by (auto simp del: sum_mod_uint64_max_le_uint64_max simp: uint64_max_def
            sum_mod_uint64_max_def
            elim!: in_set_upd_cases)
      subgoal
        unfolding bitAND_1_mod_2 one_uint64_nat_def
        by (auto simp add: init_valid_rep_in_set_iff)
      subgoal
        unfolding bitAND_1_mod_2 one_uint64_nat_def
        by (auto simp add: init_valid_rep_in_set_iff)
      subgoal
        unfolding bitAND_1_mod_2 one_uint64_nat_def
        by (auto simp add: init_valid_rep_in_set_iff)
      subgoal
        by (auto simp add: init_valid_rep_in_set_iff)
      done
    subgoal by (auto simp: uint_max_def)
    subgoal by (auto simp: uint_max_def)
    subgoal by (auto simp: uint_max_def init_next_size_def elim: neq_NilE)
    subgoal
      unfolding comp_def list_grow_def
      apply (rule RETURN_refine)
      apply (subst in_pair_collect_simp)
      apply (subst prod.case)+
      apply (intro conjI impI allI)
      subgoal
        unfolding init_next_size_def
        apply (simp del: one_uint64_nat_def)
        apply (subst init_valid_rep_insert)
        apply (auto elim: neq_NilE)
        apply (subst init_valid_rep_extend)
        apply (auto elim: neq_NilE)
        done
      subgoal by (auto simp: H Max_insert[symmetric] simp del: Max_insert)
      subgoal by (auto simp: init_next_size_def uint_max_def)
      subgoal
        using sum_mod_uint64_max_le_uint64_max
        unfolding bitOR_1_if_mod_2_nat one_uint64_nat_def
        by (auto simp del: sum_mod_uint64_max_le_uint64_max simp: uint64_max_def
            sum_mod_uint64_max_def
            elim!: in_set_upd_cases)
      subgoal by (auto simp: init_valid_rep_in_set_iff)
      subgoal by (auto simp add: init_valid_rep_in_set_iff)
      subgoal by (auto simp add: init_valid_rep_in_set_iff)
      subgoal by (auto simp add: init_valid_rep_in_set_iff)
      done
    done
qed

definition extract_atms_cls :: \<open>'a clause_l \<Rightarrow> 'a set \<Rightarrow> 'a set\<close> where
  \<open>extract_atms_cls C \<A>\<^sub>i\<^sub>n = fold (\<lambda>L \<A>\<^sub>i\<^sub>n. insert (atm_of L) \<A>\<^sub>i\<^sub>n) C \<A>\<^sub>i\<^sub>n\<close>

definition extract_atms_cls_i :: \<open>nat clause_l \<Rightarrow> nat set \<Rightarrow> nat set nres\<close> where
  \<open>extract_atms_cls_i C \<A>\<^sub>i\<^sub>n = nfoldli C (\<lambda>_. True)
       (\<lambda>L \<A>\<^sub>i\<^sub>n. do {
         ASSERT(atm_of L \<le> uint_max div 2);
         RETURN(insert (atm_of L) \<A>\<^sub>i\<^sub>n)})
    \<A>\<^sub>i\<^sub>n\<close>

lemma fild_insert_insert_swap:
  \<open>fold (\<lambda>L. insert (f L)) C (insert a \<A>\<^sub>i\<^sub>n) = insert a (fold (\<lambda>L. insert (f L)) C \<A>\<^sub>i\<^sub>n)\<close>
  by (induction C arbitrary: a \<A>\<^sub>i\<^sub>n)  (auto simp: extract_atms_cls_def)

lemma extract_atms_cls_alt_def: \<open>extract_atms_cls C \<A>\<^sub>i\<^sub>n = \<A>\<^sub>i\<^sub>n \<union> atm_of ` set C\<close>
  by (induction C)  (auto simp: extract_atms_cls_def fild_insert_insert_swap)

lemma extract_atms_cls_i_extract_atms_cls:
  \<open>(uncurry extract_atms_cls_i, uncurry (RETURN oo extract_atms_cls))
   \<in> [\<lambda>(C, \<A>\<^sub>i\<^sub>n). \<forall>L\<in>set C. nat_of_lit L \<le> uint_max]\<^sub>f
     \<langle>Id\<rangle>list_rel \<times>\<^sub>f Id \<rightarrow> \<langle>Id\<rangle>nres_rel\<close>
proof -
  have H1: \<open>(x1a, x1) \<in> \<langle>{(L, L'). L = L' \<and> nat_of_lit L \<le> uint_max}\<rangle>list_rel\<close>
    if
      \<open>case y of (C, \<A>\<^sub>i\<^sub>n) \<Rightarrow>  \<forall>L\<in>set C. nat_of_lit L \<le> uint_max\<close> and
      \<open>(x, y) \<in> \<langle>nat_lit_lit_rel\<rangle>list_rel \<times>\<^sub>f Id\<close> and
      \<open>y = (x1, x2)\<close> and
      \<open>x = (x1a, x2a)\<close>
    for x :: \<open>nat literal list \<times>  nat set\<close> and y :: \<open>nat literal list \<times>  nat set\<close> and
      x1 :: \<open>nat literal list\<close> and x2 :: \<open>nat set\<close> and x1a :: \<open>nat literal list\<close> and x2a :: \<open>nat set\<close>
    using that by (auto simp: list_rel_def list_all2_conj list.rel_eq list_all2_conv_all_nth)

  have atm_le: \<open>nat_of_lit xa \<le> uint_max \<Longrightarrow> atm_of xa \<le> uint_max div 2\<close> for xa
    by (cases xa) (auto simp: uint_max_def)

  show ?thesis
    supply RETURN_as_SPEC_refine[refine2 del]
    unfolding extract_atms_cls_i_def extract_atms_cls_def uncurry_def comp_def
      fold_eq_nfoldli
    apply (intro frefI nres_relI)
    apply (refine_rcg H1)
           apply assumption+
    subgoal by auto
    subgoal by auto
    subgoal by (auto simp: atm_le)
    subgoal by auto
    done
qed


definition extract_atms_clss:: \<open>'a clause_l list \<Rightarrow> 'a set \<Rightarrow> 'a set\<close>  where
  \<open>extract_atms_clss N \<A>\<^sub>i\<^sub>n = fold extract_atms_cls N \<A>\<^sub>i\<^sub>n\<close>

definition extract_atms_clss_i :: \<open>nat clause_l list \<Rightarrow> nat set \<Rightarrow> nat set nres\<close>  where
  \<open>extract_atms_clss_i N \<A>\<^sub>i\<^sub>n = nfoldli N (\<lambda>_. True) extract_atms_cls_i \<A>\<^sub>i\<^sub>n\<close>


lemma extract_atms_clss_i_extract_atms_clss:
  \<open>(uncurry extract_atms_clss_i, uncurry (RETURN oo extract_atms_clss))
   \<in> [\<lambda>(N, \<A>\<^sub>i\<^sub>n). \<forall>C\<in>set N. \<forall>L\<in>set C. nat_of_lit L \<le> uint_max]\<^sub>f
     \<langle>Id\<rangle>list_rel \<times>\<^sub>f Id \<rightarrow> \<langle>Id\<rangle>nres_rel\<close>
proof -
  have H1: \<open>(x1a, x1) \<in> \<langle>{(C, C'). C = C' \<and> (\<forall>L\<in>set C. nat_of_lit L \<le> uint_max)}\<rangle>list_rel\<close>
    if
      \<open>case y of (N, \<A>\<^sub>i\<^sub>n) \<Rightarrow> \<forall>C\<in>set N. \<forall>L\<in>set C. nat_of_lit L \<le> uint_max\<close> and
      \<open>(x, y) \<in> \<langle>Id\<rangle>list_rel \<times>\<^sub>f Id\<close> and
      \<open>y = (x1, x2)\<close> and
      \<open>x = (x1a, x2a)\<close>
    for x :: \<open>nat literal list list \<times> nat set\<close> and y :: \<open>nat literal list list \<times> nat set\<close> and
      x1 :: \<open>nat literal list list\<close> and x2 :: \<open>nat set\<close> and x1a :: \<open>nat literal list list\<close>
      and x2a :: \<open>nat set\<close>
    using that by (auto simp: list_rel_def list_all2_conj list.rel_eq list_all2_conv_all_nth)

  show ?thesis
    supply RETURN_as_SPEC_refine[refine2 del]
    unfolding extract_atms_clss_i_def extract_atms_clss_def comp_def fold_eq_nfoldli uncurry_def
    apply (intro frefI nres_relI)
    apply (refine_vcg H1 extract_atms_cls_i_extract_atms_cls[THEN fref_to_Down_curry,
          unfolded comp_def])
          apply assumption+
    subgoal by auto
    subgoal by auto
    subgoal by auto
    subgoal by auto
    done
qed


lemma fold_extract_atms_cls_union_swap:
  \<open>fold extract_atms_cls N (\<A>\<^sub>i\<^sub>n \<union> a) = fold extract_atms_cls N \<A>\<^sub>i\<^sub>n \<union> a\<close>
  by (induction N arbitrary: a \<A>\<^sub>i\<^sub>n)  (auto simp: extract_atms_cls_alt_def)

lemma extract_atms_clss_alt_def:
  \<open>extract_atms_clss N \<A>\<^sub>i\<^sub>n = \<A>\<^sub>i\<^sub>n \<union> ((\<Union>C\<in>set N. atm_of ` set C))\<close>
  by (induction N)
    (auto simp: extract_atms_clss_def extract_atms_cls_alt_def
      fold_extract_atms_cls_union_swap)

lemma finite_extract_atms_clss[simp]: \<open>finite (extract_atms_clss CS' {})\<close> for CS'
  by (auto simp: extract_atms_clss_alt_def)

definition op_extract_list_empty where
  \<open>op_extract_list_empty = {}\<close>

(* TODO 1024 should be replace by a proper parameter given by the parser *)
definition extract_atms_clss_imp_empty_rel where
  \<open>extract_atms_clss_imp_empty_rel = (RETURN (replicate 1024 0, 0, []))\<close>

lemma extract_atms_clss_imp_empty_rel:
  \<open>(\<lambda>_. extract_atms_clss_imp_empty_rel, \<lambda>_. (RETURN op_extract_list_empty)) \<in>
     unit_rel \<rightarrow>\<^sub>f \<langle>isasat_atms_ext_rel\<rangle> nres_rel\<close>
  by (intro frefI nres_relI)
    (simp add:  op_extract_list_empty_def uint_max_def
      isasat_atms_ext_rel_def init_valid_rep_def extract_atms_clss_imp_empty_rel_def
       del: replicate_numeral)


lemma extract_atms_cls_Nil[simp]:
  \<open>extract_atms_cls [] \<A>\<^sub>i\<^sub>n = \<A>\<^sub>i\<^sub>n\<close>
  unfolding extract_atms_cls_def fold.simps by simp

lemma extract_atms_clss_Cons[simp]:
  \<open>extract_atms_clss (C # Cs) N = extract_atms_clss Cs (extract_atms_cls C N)\<close>
  by (simp add: extract_atms_clss_def)

definition (in -) all_lits_of_atms_m :: \<open>'a multiset \<Rightarrow> 'a clause\<close> where
 \<open>all_lits_of_atms_m N = poss N + negs N\<close>

lemma (in -) all_lits_of_atms_m_nil[simp]: \<open>all_lits_of_atms_m {#} = {#}\<close>
  unfolding all_lits_of_atms_m_def by auto

definition (in -) all_lits_of_atms_mm :: \<open>'a multiset multiset \<Rightarrow> 'a clause\<close> where
 \<open>all_lits_of_atms_mm N = poss (\<Union># N) + negs (\<Union># N)\<close>

lemma all_lits_of_atms_m_all_lits_of_m:
  \<open>all_lits_of_atms_m N = all_lits_of_m (poss N)\<close>
  unfolding all_lits_of_atms_m_def all_lits_of_m_def
  by (induction N) auto


subsubsection \<open>Creation of an initial state\<close>

definition init_dt_wl_heur_spec
  :: \<open>bool \<Rightarrow> nat multiset \<Rightarrow> nat clause_l list \<Rightarrow> twl_st_wl_heur_init \<Rightarrow> twl_st_wl_heur_init \<Rightarrow> bool\<close>
where
  \<open>init_dt_wl_heur_spec unbdd \<A> CS T TOC \<longleftrightarrow>
    (\<exists>T' TOC'. (TOC, TOC') \<in> twl_st_heur_parsing_no_WL \<A> unbdd  \<and> (T, T') \<in> twl_st_heur_parsing_no_WL \<A> unbdd \<and>
        init_dt_wl_spec CS T' TOC')\<close>

definition init_state_wl :: \<open>nat twl_st_wl_init'\<close> where
  \<open>init_state_wl = ([], fmempty, None, {#}, {#}, {#})\<close>

definition init_state_wl_heur :: \<open>nat multiset \<Rightarrow> twl_st_wl_heur_init nres\<close> where
  \<open>init_state_wl_heur \<A> = do {
    M \<leftarrow> SPEC(\<lambda>M. (M, []) \<in>  trail_pol \<A>);
    D \<leftarrow> SPEC(\<lambda>D. (D, None) \<in> option_lookup_clause_rel \<A>);
    W \<leftarrow> SPEC (\<lambda>W. (W, empty_watched \<A>) \<in> \<langle>Id\<rangle>map_fun_rel (D\<^sub>0 \<A>));
    vm \<leftarrow> RES (isa_vmtf_init \<A> []);
    \<phi> \<leftarrow> SPEC (phase_saving \<A>);
    cach \<leftarrow> SPEC (cach_refinement_empty \<A>);
    let lbd = empty_lbd;
    let vdom = [];
    RETURN (M, [], D, zero_uint32_nat, W, vm, \<phi>, zero_uint32_nat, cach, lbd, vdom, False)}\<close>

definition init_state_wl_heur_fast where
  \<open>init_state_wl_heur_fast = init_state_wl_heur\<close>


lemma init_state_wl_heur_init_state_wl:
  \<open>(\<lambda>_. (init_state_wl_heur \<A>), \<lambda>_. (RETURN init_state_wl)) \<in>
   [\<lambda>_. isasat_input_bounded \<A>]\<^sub>f  unit_rel \<rightarrow> \<langle>twl_st_heur_parsing_no_WL_wl \<A> unbdd\<rangle>nres_rel\<close>
  by (intro frefI nres_relI)
    (auto simp: init_state_wl_heur_def init_state_wl_def
        RES_RETURN_RES bind_RES_RETURN_eq RES_RES_RETURN_RES RETURN_def
        twl_st_heur_parsing_no_WL_wl_def vdom_m_def empty_watched_def valid_arena_empty
        intro!: RES_refine)

definition (in -)to_init_state :: \<open>nat twl_st_wl_init' \<Rightarrow> nat twl_st_wl_init\<close> where
  \<open>to_init_state S = (S, {#})\<close>

definition (in -) from_init_state :: \<open>nat twl_st_wl_init_full \<Rightarrow> nat twl_st_wl\<close> where
  \<open>from_init_state = fst\<close>

(*
lemma id_to_init_state:
  \<open>(RETURN o id, RETURN o to_init_state) \<in> twl_st_heur_parsing_no_WL_wl \<rightarrow>\<^sub>f \<langle>twl_st_heur_parsing_no_WL_wl_no_watched_full\<rangle>nres_rel\<close>
  by (intro frefI nres_relI)
    (auto simp: to_init_state_def twl_st_heur_parsing_no_WL_wl_def twl_st_heur_parsing_no_WL_wl_no_watched_full_def
      twl_st_heur_parsing_no_WL_def)
*)

definition (in -) to_init_state_code where
  \<open>to_init_state_code = id\<close>


definition from_init_state_code where
  \<open>from_init_state_code = id\<close>

definition (in -) conflict_is_None_heur_wl where
  \<open>conflict_is_None_heur_wl = (\<lambda>(M, N, U, D, _). is_None D)\<close>

definition (in -) finalise_init where
  \<open>finalise_init = id\<close>


subsection \<open>Parsing\<close>

lemma init_dt_wl_heur_init_dt_wl:
  \<open>(uncurry (init_dt_wl_heur unbdd), uncurry init_dt_wl) \<in>
    [\<lambda>(CS, S). (\<forall>C \<in> set CS. literals_are_in_\<L>\<^sub>i\<^sub>n \<A> (mset C)) \<and> distinct_mset_set (mset ` set CS)]\<^sub>f
     \<langle>Id\<rangle>list_rel \<times>\<^sub>f twl_st_heur_parsing_no_WL \<A> unbdd \<rightarrow> \<langle>twl_st_heur_parsing_no_WL \<A> unbdd\<rangle> nres_rel\<close>
proof -
  have H: \<open>\<And>x y x1 x2 x1a x2a.
       (\<forall>C\<in>set x1. literals_are_in_\<L>\<^sub>i\<^sub>n \<A> (mset C)) \<and> distinct_mset_set (mset ` set x1) \<Longrightarrow>
       (x1a, x1) \<in> \<langle>Id\<rangle>list_rel \<Longrightarrow>
       (x1a, x1) \<in> \<langle>{(C, C'). C = C' \<and> literals_are_in_\<L>\<^sub>i\<^sub>n \<A> (mset C) \<and>
          distinct C}\<rangle>list_rel\<close>
    apply (auto simp: list_rel_def list_all2_conj)
    apply (auto simp: list_all2_conv_all_nth distinct_mset_set_def)
    done

  show ?thesis
    unfolding init_dt_wl_heur_def init_dt_wl_def uncurry_def
    apply (intro frefI nres_relI)
    apply (case_tac y rule: prod.exhaust)
    apply (case_tac x rule: prod.exhaust)
    apply (simp only: prod.case prod_rel_iff)
    apply (refine_vcg init_dt_step_wl_heur_init_dt_step_wl[THEN fref_to_Down_curry] H)
         apply normalize_goal+
    subgoal by fast
    subgoal by fast
    subgoal by simp
    subgoal by auto
    subgoal by auto
    subgoal by auto
    subgoal by auto
    subgoal by (auto simp: twl_st_heur_parsing_no_WL_def)
    done
qed

definition rewatch_heur_st
 :: \<open>twl_st_wl_heur_init \<Rightarrow> twl_st_wl_heur_init nres\<close>
where
\<open>rewatch_heur_st = (\<lambda>(M', N', D', j, W, vm, \<phi>, clvls, cach, lbd, vdom, failed). do {
    ASSERT(length vdom \<le> length N');
    W \<leftarrow> rewatch_heur vdom N' W;
    RETURN (M', N', D', j, W, vm, \<phi>, clvls, cach, lbd, vdom, failed)
  })\<close>

lemma rewatch_heur_st_correct_watching:
  assumes
    \<open>(S, T) \<in> twl_st_heur_parsing_no_WL \<A> unbdd\<close> and failed: \<open>\<not>is_failed_heur_init S\<close>
    \<open>literals_are_in_\<L>\<^sub>i\<^sub>n_mm \<A> (mset `# ran_mf (get_clauses_init_wl T))\<close> and
    \<open>\<And>x. x \<in># dom_m (get_clauses_init_wl T) \<Longrightarrow> distinct (get_clauses_init_wl T \<propto> x) \<and>
        2 \<le> length (get_clauses_init_wl T \<propto> x)\<close>
  shows \<open>rewatch_heur_st S \<le> \<Down> (twl_st_heur_parsing \<A> unbdd)
    (SPEC (\<lambda>((M,N, D, NE, UE, Q, W), OC). T = ((M,N,D,NE,UE,Q), OC)\<and>
       correct_watching (M, N, D, NE, UE, Q, W)))\<close>
proof -
  obtain M N D NE UE Q OC where
    T: \<open>T = ((M,N, D, NE, UE, Q), OC)\<close>
    by (cases T) auto

  obtain M' N' D' j W vm \<phi> clvls cach lbd vdom where
    S: \<open>S = (M', N', D', j, W, vm, \<phi>, clvls, cach, lbd, vdom, False)\<close>
    using failed by (cases S) auto

  have valid: \<open>valid_arena N' N (set vdom)\<close> and
    dist: \<open>distinct vdom\<close> and
    dom_m_vdom: \<open>set_mset (dom_m N) \<subseteq> set vdom\<close> and
    W: \<open>(W, empty_watched \<A>) \<in> \<langle>Id\<rangle>map_fun_rel (D\<^sub>0 \<A>)\<close> and
    lits: \<open>literals_are_in_\<L>\<^sub>i\<^sub>n_mm \<A> (mset `# ran_mf N)\<close>
    using assms distinct_mset_dom[of N] apply (auto simp: twl_st_heur_parsing_no_WL_def S T
      simp flip: distinct_mset_mset_distinct)
    by (metis distinct_mset_set_mset_ident set_mset_mset subset_mset.eq_iff)+
  have H: \<open>RES ({(W, W').
          (W, W') \<in> \<langle>Id\<rangle>map_fun_rel (D\<^sub>0 \<A>) \<and> vdom_m \<A> W' N \<subseteq> set_mset (dom_m N)}\<inverse> ``
         {W. Watched_Literals_Watch_List_Initialisation.correct_watching_init
              (M, N, D, NE, UE, Q, W)})
    \<le> RES ({(W, W').
          (W, W') \<in> \<langle>Id\<rangle>map_fun_rel (D\<^sub>0 \<A>) \<and> vdom_m \<A> W' N \<subseteq> set_mset (dom_m N)}\<inverse> ``
         {W. Watched_Literals_Watch_List_Initialisation.correct_watching_init
              (M, N, D, NE, UE, Q, W)})\<close>
    for W'
    by (rule order.refl)
  have eq: \<open>Watched_Literals_Watch_List_Initialisation.correct_watching_init
        (M, N, None, NE, UE, {#}, xa) \<Longrightarrow>
       vdom_m \<A> xa N = set_mset (dom_m N)\<close> for xa
    by (auto 5 5 simp: Watched_Literals_Watch_List_Initialisation.correct_watching_init.simps
      vdom_m_def)
  show ?thesis
    supply [[goals_limit=1]]
    using assms
    unfolding rewatch_heur_st_def T S
    apply clarify
    apply (rule ASSERT_leI)
    subgoal by (auto dest: valid_arena_vdom_subset simp: twl_st_heur_parsing_no_WL_def)
      apply (rule bind_refine_res)
      prefer 2
      apply (rule order.trans)
      apply (rule rewatch_heur_rewatch[OF valid _ dist dom_m_vdom W lits])
      apply (solves simp)
      apply (solves simp)
      apply (rule order_trans[OF ref_two_step'])
      apply (rule rewatch_correctness)
      apply (rule empty_watched_def)
      subgoal
        using assms
        by (auto simp: twl_st_heur_parsing_no_WL_def)
      apply (subst conc_fun_RES)
      apply (rule H) apply (rule RETURN_RES_refine)
      apply (auto simp: twl_st_heur_parsing_def twl_st_heur_parsing_no_WL_def all_atms_def[symmetric]
        intro!: exI[of _ N] exI[of _ D]  exI[of _ M]
        intro!: )
      apply (rule_tac x=W' in exI)
      apply (auto simp: eq correct_watching_init_correct_watching dist)
      apply (rule_tac x=W' in exI)
      apply (auto simp: eq correct_watching_init_correct_watching dist)
      done
qed


subsubsection \<open>Full Initialisation\<close>


definition rewatch_heur_st_fast where
  \<open>rewatch_heur_st_fast = rewatch_heur_st\<close>

definition rewatch_heur_st_fast_pre where
  \<open>rewatch_heur_st_fast_pre S =
     ((\<forall>x \<in> set (get_vdom_heur_init S). x \<le> uint64_max) \<and> length (get_clauses_wl_heur_init S) \<le> uint64_max)\<close>

definition init_dt_wl_heur_full
  :: \<open>bool \<Rightarrow> _ \<Rightarrow> twl_st_wl_heur_init \<Rightarrow> twl_st_wl_heur_init nres\<close>
where
\<open>init_dt_wl_heur_full unb CS S = do {
    S \<leftarrow> init_dt_wl_heur unb CS S;
    ASSERT(\<not>is_failed_heur_init S);
    rewatch_heur_st S
  }\<close>

definition init_dt_wl_heur_full_unb
  :: \<open>_ \<Rightarrow> twl_st_wl_heur_init \<Rightarrow> twl_st_wl_heur_init nres\<close>
where
\<open>init_dt_wl_heur_full_unb = init_dt_wl_heur_full True\<close>

lemma init_dt_wl_heur_full_init_dt_wl_full:
  assumes
    \<open>init_dt_wl_pre CS T\<close> and
    \<open>\<forall>C\<in>set CS. literals_are_in_\<L>\<^sub>i\<^sub>n \<A> (mset C)\<close> and
    \<open>distinct_mset_set (mset ` set CS)\<close> and
    \<open>(S, T) \<in> twl_st_heur_parsing_no_WL \<A> True\<close>
  shows \<open>init_dt_wl_heur_full True CS S
         \<le> \<Down> (twl_st_heur_parsing \<A> True) (init_dt_wl_full CS T)\<close>
proof -
  have H: \<open>valid_arena x1g x1b (set x1p)\<close> \<open>set x1p \<subseteq> set x1p\<close> \<open>set_mset (dom_m x1b) \<subseteq> set x1p\<close>
    \<open>distinct x1p\<close> \<open>(x1j, \<lambda>_. []) \<in> \<langle>Id\<rangle>map_fun_rel (D\<^sub>0 \<A>)\<close>
    if
      xx': \<open>(x, x') \<in> twl_st_heur_parsing_no_WL \<A> True\<close> and
      st: \<open>x2c = (x1e, x2d)\<close>
        \<open>x2b = (x1d, x2c)\<close>
        \<open>x2a = (x1c, x2b)\<close>
        \<open>x2 = (x1b, x2a)\<close>
        \<open>x1 = (x1a, x2)\<close>
        \<open>x' = (x1, x2e)\<close>
        \<open>x2o = (x1p, x2p)\<close>
        \<open>x2n = (x1o, x2o)\<close>
        \<open>x2m = (x1n, x2n)\<close>
        \<open>x2l = (x1m, x2m)\<close>
        \<open>x2k = (x1l, x2l)\<close>
        \<open>x2j = (x1k, x2k)\<close>
        \<open>x2i = (x1j, x2j)\<close>
        \<open>x2h = (x1i, x2i)\<close>
        \<open>x2g = (x1h, x2h)\<close>
        \<open>x2f = (x1g, x2g)\<close>
        \<open>x = (x1f, x2f)\<close>
    for x x' x1 x1a x2 x1b x2a x1c x2b x1d x2c x1e x2d x2e x1f x2f x1g x2g x1h x2h
       x1i x2i x1j x2j x1k x2k x1l x2l x1m x2m x1n x2n x1o x2o x1p x2p
  proof -
    show \<open>valid_arena x1g x1b (set x1p)\<close> \<open>set x1p \<subseteq> set x1p\<close> \<open>set_mset (dom_m x1b) \<subseteq> set x1p\<close>
      \<open>distinct x1p\<close> \<open>(x1j, \<lambda>_. []) \<in> \<langle>Id\<rangle>map_fun_rel (D\<^sub>0 \<A>)\<close>
    using xx' distinct_mset_dom[of x1b] unfolding st
      by (auto simp: twl_st_heur_parsing_no_WL_def empty_watched_def
        simp flip: set_mset_mset distinct_mset_mset_distinct)
  qed

  show ?thesis
    unfolding init_dt_wl_heur_full_def init_dt_wl_full_def rewatch_heur_st_def
    apply (refine_rcg rewatch_heur_rewatch[of _ _ _ _ _ _ \<A>]
      init_dt_wl_heur_init_dt_wl[of True \<A>, THEN fref_to_Down_curry])
    subgoal using assms by fast
    subgoal using assms by fast
    subgoal using assms by auto
    subgoal by (auto simp: twl_st_heur_parsing_def twl_st_heur_parsing_no_WL_def)
    subgoal by (auto dest: valid_arena_vdom_subset simp: twl_st_heur_parsing_no_WL_def)
    apply ((rule H; assumption)+)[5]
    subgoal
      by (auto simp: twl_st_heur_parsing_def twl_st_heur_parsing_no_WL_def
      literals_are_in_\<L>\<^sub>i\<^sub>n_mm_def all_lits_of_mm_union)
    subgoal by (auto simp: twl_st_heur_parsing_def twl_st_heur_parsing_no_WL_def
      empty_watched_def[symmetric] map_fun_rel_def vdom_m_def)
    subgoal by (auto simp: twl_st_heur_parsing_def twl_st_heur_parsing_no_WL_def
      empty_watched_def[symmetric])
    done
qed


lemma init_dt_wl_heur_full_init_dt_wl_spec_full:
  assumes
    \<open>init_dt_wl_pre CS T\<close> and
    \<open>\<forall>C\<in>set CS. literals_are_in_\<L>\<^sub>i\<^sub>n \<A> (mset C)\<close> and
    \<open>distinct_mset_set (mset ` set CS)\<close> and
    \<open>(S, T) \<in> twl_st_heur_parsing_no_WL \<A> True\<close>
  shows \<open>init_dt_wl_heur_full True CS S
      \<le>  \<Down> (twl_st_heur_parsing \<A> True) (SPEC (init_dt_wl_spec_full CS T))\<close>
  apply (rule order.trans)
  apply (rule init_dt_wl_heur_full_init_dt_wl_full[OF assms])
  apply (rule ref_two_step')
  apply (rule init_dt_wl_full_init_dt_wl_spec_full[OF assms(1)])
  done


subsection \<open>Conversion to normal state\<close>

  (*
definition (in -)insert_sort_inner_nth2 :: \<open>nat list \<Rightarrow> nat list \<Rightarrow> nat \<Rightarrow> nat list nres\<close> where
  \<open>insert_sort_inner_nth2 ns = insert_sort_inner (>) (\<lambda>remove n. ns ! (remove ! n))\<close>

definition (in -)insert_sort_nth2 :: \<open>nat list \<Rightarrow> nat list \<Rightarrow> nat list nres\<close> where
  [code del]: \<open>insert_sort_nth2 = (\<lambda>ns. insert_sort (>) (\<lambda>remove n. ns ! (remove ! n)))\<close>

sepref_definition (in -) insert_sort_inner_nth_code
   is \<open>uncurry2 insert_sort_inner_nth2\<close>
   :: \<open>[\<lambda>((xs, vars), n). (\<forall>x\<in>#mset vars. x < length xs) \<and> n < length vars]\<^sub>a
  (array_assn uint64_nat_assn)\<^sup>k *\<^sub>a (arl_assn uint32_nat_assn)\<^sup>d *\<^sub>a nat_assn\<^sup>k \<rightarrow>
  arl_assn uint32_nat_assn\<close>
  unfolding insert_sort_inner_nth2_def insert_sort_inner_def fast_minus_def[symmetric]
    short_circuit_conv
  supply [[goals_limit = 1]]
  supply mset_eq_setD[dest] mset_eq_length[dest]
    if_splits[split]
  by sepref


declare insert_sort_inner_nth_code.refine[sepref_fr_rules]

sepref_definition (in -) insert_sort_nth_code
   is \<open>uncurry insert_sort_nth2\<close>
   :: \<open>[\<lambda>(xs, vars). (\<forall>x\<in>#mset vars. x < length xs)]\<^sub>a
      (array_assn uint64_nat_assn)\<^sup>k *\<^sub>a (arl_assn uint32_nat_assn)\<^sup>d  \<rightarrow>
       arl_assn uint32_nat_assn\<close>
  unfolding insert_sort_nth2_def insert_sort_def insert_sort_inner_nth2_def[symmetric]
  supply [[goals_limit = 1]]
  supply mset_eq_setD[dest] mset_eq_length[dest]
  by sepref

declare insert_sort_nth_code.refine[sepref_fr_rules]

declare insert_sort_nth2_def[unfolded insert_sort_def insert_sort_inner_def, code]
*)
definition extract_lits_sorted where
  \<open>extract_lits_sorted = (\<lambda>(xs, n, vars). do {
    vars \<leftarrow> \<comment>\<open>insert\_sort\_nth2 xs vars\<close>RETURN vars;
    RETURN (vars, n)
  })\<close>


definition lits_with_max_rel where
  \<open>lits_with_max_rel = {((xs, n), \<A>\<^sub>i\<^sub>n). mset xs = \<A>\<^sub>i\<^sub>n \<and> n = Max (insert 0 (set xs)) \<and>
    length xs < uint32_max}\<close>

lemma extract_lits_sorted_mset_set:
  \<open>(extract_lits_sorted, RETURN o mset_set)
   \<in> isasat_atms_ext_rel \<rightarrow>\<^sub>f \<langle>lits_with_max_rel\<rangle>nres_rel\<close>
proof -
  have K: \<open>RETURN o mset_set = (\<lambda>v. do {v' \<leftarrow> SPEC(\<lambda>v'. v' = mset_set v); RETURN v'})\<close>
    by auto
  have K': \<open>length x2a < uint32_max\<close> if \<open>distinct b\<close> \<open>init_valid_rep x1 (set b)\<close>
    \<open>length x1 < uint_max\<close> \<open>mset x2a = mset b\<close>for x1 x2a b
  proof -
    have \<open>distinct x2a\<close>
      by (simp add: same_mset_distinct_iff that(1) that(4))
    have \<open>length x2a = length b\<close> \<open>set x2a = set b\<close>
      using \<open>mset x2a = mset b\<close> apply (metis size_mset)
       using \<open>mset x2a = mset b\<close> by (rule mset_eq_setD)
    then have \<open>set x2a \<subseteq> {0..<uint_max - 1}\<close>
      using that by (auto simp: init_valid_rep_def)
    from card_mono[OF _ this] show ?thesis
      using \<open>distinct x2a\<close> by (auto simp: uint_max_def distinct_card)
  qed
  have H_simple: \<open>RETURN x2a
      \<le> \<Down> (list_mset_rel \<inter> {(v, v'). length v < uint_max})
          (SPEC (\<lambda>v'. v' = mset_set y))\<close>
    if
      \<open>(x, y) \<in> isasat_atms_ext_rel\<close> and
      \<open>x2 = (x1a, x2a)\<close> and
      \<open>x = (x1, x2)\<close>
    for x :: \<open>nat list \<times> nat \<times> nat list\<close> and y :: \<open>nat set\<close> and x1 :: \<open>nat list\<close> and
      x2 :: \<open>nat \<times> nat list\<close> and x1a :: \<open>nat\<close> and x2a :: \<open>nat list\<close>
    using that mset_eq_length by (auto simp: isasat_atms_ext_rel_def list_mset_rel_def br_def
          mset_set_set RETURN_def intro: K' intro!: RES_refine dest: mset_eq_length)

  show ?thesis
    unfolding extract_lits_sorted_def reorder_list_def K
    apply (intro frefI nres_relI)
    apply (refine_vcg H_simple)
       apply assumption+
    by (auto simp: lits_with_max_rel_def isasat_atms_ext_rel_def mset_set_set list_mset_rel_def
        br_def dest!: mset_eq_setD)
qed

text \<open>TODO Move\<close>

text \<open>The value 160 is random (but larger than the default 16 for array lists).\<close>
definition finalise_init_code :: \<open>opts \<Rightarrow> twl_st_wl_heur_init \<Rightarrow> twl_st_wl_heur nres\<close> where
  \<open>finalise_init_code opts =
    (\<lambda>(M', N', D', Q', W', ((ns, m, fst_As, lst_As, next_search), to_remove), \<phi>, clvls, cach,
       lbd, vdom, _). do {
     ASSERT(lst_As \<noteq> None \<and> fst_As \<noteq> None);
     let init_stats = (0::uint64, 0::uint64, 0::uint64, 0::uint64, 0::uint64, 0::uint64, 0::uint64, 0::uint64);
     let fema = ema_fast_init;
     let sema = ema_slow_init;
     let ccount = restart_info_init;
     let lcount = zero_uint64_nat;
    RETURN (M', N', D', Q', W', ((ns, m, the fst_As, the lst_As, next_search), to_remove), \<phi>,
       clvls, cach, lbd, take 1(replicate 160 (Pos zero_uint32_nat)), init_stats,
        fema, sema, ccount, vdom, [], lcount, opts, [])
     })\<close>

lemma isa_vmtf_init_nemptyD: \<open>((ak, al, am, an, bc), ao, bd)
       \<in> isa_vmtf_init \<A> au \<Longrightarrow> \<A> \<noteq> {#} \<Longrightarrow>  \<exists>y. an = Some y\<close>
     \<open>((ak, al, am, an, bc), ao, bd)
       \<in> isa_vmtf_init \<A> au \<Longrightarrow> \<A> \<noteq> {#} \<Longrightarrow>  \<exists>y. am = Some y\<close>
   by (auto simp: isa_vmtf_init_def vmtf_init_def)

lemma isa_vmtf_init_isa_vmtf: \<open>\<A> \<noteq> {#} \<Longrightarrow> ((ak, al, Some am, Some an, bc), ao, bd)
       \<in> isa_vmtf_init \<A> au \<Longrightarrow> ((ak, al, am, an, bc), ao, bd)
       \<in> isa_vmtf \<A> au\<close>
  by (auto simp: isa_vmtf_init_def vmtf_init_def Image_iff intro!: isa_vmtfI)

lemma finalise_init_finalise_init_full:
  \<open>get_conflict_wl S = None \<Longrightarrow>
  all_atms_st S \<noteq> {#} \<Longrightarrow> size (learned_clss_l (get_clauses_wl S)) = 0 \<Longrightarrow>
  ((ops', T), ops, S) \<in> Id \<times>\<^sub>f twl_st_heur_post_parsing_wl True \<Longrightarrow>
  finalise_init_code ops' T \<le> \<Down> {(S', T'). (S', T') \<in> twl_st_heur \<and>
    get_clauses_wl_heur_init T = get_clauses_wl_heur S'} (RETURN (finalise_init S))\<close>
  by (auto 5 5 simp: finalise_init_def twl_st_heur_def twl_st_heur_parsing_no_WL_def
    twl_st_heur_parsing_no_WL_wl_def
      finalise_init_code_def out_learned_def all_atms_def
      twl_st_heur_post_parsing_wl_def
      intro!: ASSERT_leI intro!: isa_vmtf_init_isa_vmtf
      dest: isa_vmtf_init_nemptyD)

lemma finalise_init_finalise_init:
  \<open>(uncurry finalise_init_code, uncurry (RETURN oo (\<lambda>_. finalise_init))) \<in>
   [\<lambda>(_, S::nat twl_st_wl). get_conflict_wl S = None \<and> all_atms_st S \<noteq> {#} \<and>
      size (learned_clss_l (get_clauses_wl S)) = 0]\<^sub>f Id \<times>\<^sub>r
      twl_st_heur_post_parsing_wl True \<rightarrow> \<langle>twl_st_heur\<rangle>nres_rel\<close>
  by (intro frefI nres_relI)
    (auto 5 5 simp: finalise_init_def twl_st_heur_def twl_st_heur_parsing_no_WL_def twl_st_heur_parsing_no_WL_wl_def
      finalise_init_code_def out_learned_def all_atms_def
      twl_st_heur_post_parsing_wl_def
      intro!: ASSERT_leI intro!: isa_vmtf_init_isa_vmtf
      dest: isa_vmtf_init_nemptyD)

definition (in -) init_rll :: \<open>nat \<Rightarrow> (nat, 'v clause_l \<times> bool) fmap\<close> where
  \<open>init_rll n = fmempty\<close>

definition (in -) init_aa :: \<open>nat \<Rightarrow> 'v list\<close> where
  \<open>init_aa n = []\<close>


definition (in -) init_aa' :: \<open>nat \<Rightarrow> (clause_status \<times> nat \<times> nat) list\<close> where
  \<open>init_aa' n = []\<close>


definition init_trail_D :: \<open>uint32 list \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> trail_pol nres\<close> where
  \<open>init_trail_D \<A>\<^sub>i\<^sub>n n m = do {
     let M0 = [];
     let cs = [];
     let M = replicate m UNSET;
     let M' = replicate n zero_uint32_nat;
     let M'' = replicate n 1;
     RETURN ((M0, M, M', M'', zero_uint32_nat, cs))
  }\<close>

definition init_trail_D_fast where
  \<open>init_trail_D_fast = init_trail_D\<close>


definition init_state_wl_D' :: \<open>uint32 list \<times> uint32 \<Rightarrow>  (trail_pol \<times> _ \<times> _) nres\<close> where
  \<open>init_state_wl_D' = (\<lambda>(\<A>\<^sub>i\<^sub>n, n). do {
     ASSERT(Suc (2 * (nat_of_uint32 n)) \<le> uint32_max);
     let n = Suc (nat_of_uint32 n);
     let m = 2 * n;
     M \<leftarrow> init_trail_D \<A>\<^sub>i\<^sub>n n m;
     let N = [];
     let D = (True, zero_uint32_nat, replicate n NOTIN);
     let WS = replicate m [];
     vm \<leftarrow> initialise_VMTF \<A>\<^sub>i\<^sub>n n;
     let \<phi> = replicate n False;
     let cach = (replicate n SEEN_UNKNOWN, []);
     let lbd = empty_lbd;
     let vdom = [];
     RETURN (M, N, D, zero_uint32_nat, WS, vm, \<phi>, zero_uint32_nat, cach, lbd, vdom, False)
  })\<close>

lemma init_trail_D_ref:
  \<open>(uncurry2 init_trail_D, uncurry2 (RETURN ooo (\<lambda> _ _ _. []))) \<in> [\<lambda>((N, n), m). mset N = \<A>\<^sub>i\<^sub>n \<and>
    distinct N \<and> (\<forall>L\<in>set N. L < n) \<and> m = 2 * n \<and> isasat_input_bounded \<A>\<^sub>i\<^sub>n]\<^sub>f
    \<langle>uint32_nat_rel\<rangle>list_rel \<times>\<^sub>f nat_rel \<times>\<^sub>f nat_rel \<rightarrow>
   \<langle>trail_pol \<A>\<^sub>i\<^sub>n\<rangle> nres_rel\<close>
proof -
  have K: \<open>(\<forall>L\<in>set N. nat_of_uint32 L < n) \<longleftrightarrow>
     (\<forall>L \<in># (\<L>\<^sub>a\<^sub>l\<^sub>l (nat_of_uint32 `# mset N)). atm_of L < n)\<close> for N n
    apply (rule iffI)
    subgoal by (auto simp: in_\<L>\<^sub>a\<^sub>l\<^sub>l_atm_of_\<A>\<^sub>i\<^sub>n)
    subgoal by (metis (full_types) image_eqI in_\<L>\<^sub>a\<^sub>l\<^sub>l_atm_of_\<A>\<^sub>i\<^sub>n literal.sel(1)
          set_image_mset set_mset_mset)
    done
  have K': \<open>(\<forall>L\<in>set N. L < n) \<Longrightarrow>
     (\<forall>L \<in># (\<L>\<^sub>a\<^sub>l\<^sub>l (mset N)). nat_of_lit L < 2 * n)\<close>
     (is \<open>?A \<Longrightarrow> ?B\<close>) for N n
     (*TODO proof*)
  proof -
    assume ?A
    then show ?B
      apply (auto simp: in_\<L>\<^sub>a\<^sub>l\<^sub>l_atm_of_\<A>\<^sub>i\<^sub>n)
      apply (case_tac L)
      apply auto
      done
  qed
  show ?thesis
    unfolding init_trail_D_def
    apply (intro frefI nres_relI)
    unfolding uncurry_def Let_def comp_def trail_pol_def
    apply clarify
    unfolding RETURN_refine_iff
    apply clarify
    apply (intro conjI)
    subgoal
      by (auto simp: ann_lits_split_reasons_def
          list_mset_rel_def Collect_eq_comp list_rel_def
          list_all2_op_eq_map_right_iff' uint32_nat_rel_def
          br_def in_\<L>\<^sub>a\<^sub>l\<^sub>l_atm_of_in_atms_of_iff atms_of_\<L>\<^sub>a\<^sub>l\<^sub>l_\<A>\<^sub>i\<^sub>n
        dest: multi_member_split)
    subgoal
      by auto
    subgoal using K' by (auto simp: polarity_def)
    subgoal
      by (auto simp: zero_uint32_def shiftr1_def
        nat_shiftr_div2 nat_of_uint32_shiftr in_\<L>\<^sub>a\<^sub>l\<^sub>l_atm_of_in_atms_of_iff
        polarity_atm_def trail_pol_def K
        phase_saving_def list_rel_mset_rel_def  atms_of_\<L>\<^sub>a\<^sub>l\<^sub>l_\<A>\<^sub>i\<^sub>n
        list_rel_def uint32_nat_rel_def br_def list_all2_op_eq_map_right_iff'
        ann_lits_split_reasons_def
      list_mset_rel_def Collect_eq_comp)
    subgoal
      by auto
    subgoal
      by auto
    subgoal
      by (auto simp: control_stack.empty)
    subgoal by auto
    done
qed

(*TODO Move *)
definition [to_relAPP]: "mset_rel A \<equiv> p2rel (rel_mset (rel2p A))"
lemma in_mset_rel_eq_f_iff:
  \<open>(a, b) \<in> \<langle>{(c, a). a = f c}\<rangle>mset_rel \<longleftrightarrow> b = f `# a\<close>
  using ex_mset[of a]
  by (auto simp: mset_rel_def br_def rel2p_def[abs_def] p2rel_def rel_mset_def
      list_all2_op_eq_map_right_iff' cong: ex_cong)


lemma in_mset_rel_eq_f_iff_set:
  \<open>\<langle>{(c, a). a = f c}\<rangle>mset_rel = {(b, a). a = f `# b}\<close>
  using in_mset_rel_eq_f_iff[of _ _ f] by blast
(*END Move*)
lemma init_state_wl_D0:
  \<open>(init_state_wl_D', init_state_wl_heur) \<in>
    [\<lambda>N. N = \<A>\<^sub>i\<^sub>n \<and> distinct_mset \<A>\<^sub>i\<^sub>n \<and> isasat_input_bounded \<A>\<^sub>i\<^sub>n]\<^sub>f
      lits_with_max_rel O \<langle>uint32_nat_rel\<rangle>mset_rel \<rightarrow>
      \<langle>Id \<times>\<^sub>r Id \<times>\<^sub>r
         Id \<times>\<^sub>r nat_rel \<times>\<^sub>r \<langle>\<langle>Id\<rangle>list_rel\<rangle>list_rel \<times>\<^sub>r
           Id \<times>\<^sub>r \<langle>bool_rel\<rangle>list_rel \<times>\<^sub>r Id \<times>\<^sub>r Id \<times>\<^sub>r Id\<rangle>nres_rel\<close>
  (is \<open>?C \<in> [?Pre]\<^sub>f ?arg \<rightarrow> \<langle>?im\<rangle>nres_rel\<close>)
proof -
  have init_state_wl_heur_alt_def: \<open>init_state_wl_heur \<A>\<^sub>i\<^sub>n = do {
    M \<leftarrow> SPEC (\<lambda>M. (M, []) \<in> trail_pol \<A>\<^sub>i\<^sub>n);
    N \<leftarrow> RETURN [];
    D \<leftarrow> SPEC (\<lambda>D. (D, None) \<in> option_lookup_clause_rel \<A>\<^sub>i\<^sub>n);
    W \<leftarrow> SPEC (\<lambda>W. (W, empty_watched \<A>\<^sub>i\<^sub>n ) \<in> \<langle>Id\<rangle>map_fun_rel (D\<^sub>0 \<A>\<^sub>i\<^sub>n));
    vm \<leftarrow> RES (isa_vmtf_init \<A>\<^sub>i\<^sub>n []);
    \<phi> \<leftarrow> SPEC (phase_saving \<A>\<^sub>i\<^sub>n);
    cach \<leftarrow> SPEC (cach_refinement_empty \<A>\<^sub>i\<^sub>n);
    let lbd = empty_lbd;
    let vdom = [];
    RETURN (M, N, D, 0, W, vm, \<phi>, zero_uint32_nat, cach, lbd, vdom, False)}\<close> for \<A>\<^sub>i\<^sub>n
    unfolding init_state_wl_heur_def Let_def by auto

  have tr: \<open>distinct_mset \<A>\<^sub>i\<^sub>n \<and> (\<forall>L\<in>#\<A>\<^sub>i\<^sub>n. L < b) \<Longrightarrow>
        (\<A>\<^sub>i\<^sub>n', \<A>\<^sub>i\<^sub>n) \<in> \<langle>uint32_nat_rel\<rangle>list_rel_mset_rel \<Longrightarrow> isasat_input_bounded \<A>\<^sub>i\<^sub>n \<Longrightarrow>
     b' = 2 * b \<Longrightarrow>
      init_trail_D \<A>\<^sub>i\<^sub>n' b (2 * b) \<le> \<Down> (trail_pol \<A>\<^sub>i\<^sub>n) (RETURN [])\<close> for b' b \<A>\<^sub>i\<^sub>n \<A>\<^sub>i\<^sub>n' x
    by (rule init_trail_D_ref[unfolded fref_def nres_rel_def, simplified, rule_format])
      (auto simp: list_rel_mset_rel_def list_mset_rel_def br_def)

  have [simp]: \<open>comp_fun_idem (max :: 'a :: {zero,linorder} \<Rightarrow> _)\<close>
    unfolding comp_fun_idem_def comp_fun_commute_def comp_fun_idem_axioms_def
    by (auto simp: max_def[abs_def] intro!: ext)
  have [simp]: \<open>fold max x a = Max (insert a (set x))\<close> for x and a :: \<open>'a :: {zero,linorder}\<close>
    by (auto simp: Max.eq_fold comp_fun_idem.fold_set_fold)
  have in_N0: \<open>L \<in> set \<A>\<^sub>i\<^sub>n \<Longrightarrow> nat_of_uint32 L  < Suc (nat_of_uint32 (Max (insert 0 (set \<A>\<^sub>i\<^sub>n))))\<close>
    for L \<A>\<^sub>i\<^sub>n
    using Max_ge[of \<open>insert 0 (set \<A>\<^sub>i\<^sub>n)\<close> L]
    apply (auto simp del: Max_ge simp: nat_shiftr_div2 nat_of_uint32_shiftr)
    using div_le_mono le_imp_less_Suc nat_of_uint32_le_iff by blast
  define P where \<open>P x = {(a, b). b = [] \<and> (a, b) \<in> trail_pol x}\<close> for x
  have P: \<open>(c, []) \<in> P x \<longleftrightarrow> (c, []) \<in> trail_pol x\<close> for c x
    unfolding P_def by auto
  have [simp]: \<open>\<And>a \<A>\<^sub>i\<^sub>n. (a, \<A>\<^sub>i\<^sub>n) \<in> \<langle>uint32_nat_rel\<rangle>mset_rel \<longleftrightarrow> \<A>\<^sub>i\<^sub>n = nat_of_uint32 `# a\<close>
    by (auto simp: uint32_nat_rel_def br_def in_mset_rel_eq_f_iff list_rel_mset_rel_def)
  have [simp]: \<open>(a, nat_of_uint32 `# mset a) \<in> \<langle>uint32_nat_rel\<rangle>list_rel_mset_rel\<close> for a
    unfolding list_rel_mset_rel_def
    by (rule relcompI [of _ \<open>map nat_of_uint32 a\<close>])
       (auto simp: list_rel_def uint32_nat_rel_def br_def list_all2_op_eq_map_right_iff'
        list_mset_rel_def)
  have init: \<open>init_trail_D x1 (Suc (nat_of_uint32 x2))
          (2 * Suc (nat_of_uint32 x2)) \<le>
     SPEC (\<lambda>c. (c, []) \<in> trail_pol \<A>\<^sub>i\<^sub>n)\<close>
    if \<open>distinct_mset \<A>\<^sub>i\<^sub>n\<close> and x: \<open>(\<A>\<^sub>i\<^sub>n', \<A>\<^sub>i\<^sub>n) \<in> ?arg\<close> and
      \<open>\<A>\<^sub>i\<^sub>n' = (x1, x2)\<close> and \<open>isasat_input_bounded \<A>\<^sub>i\<^sub>n\<close>
    for \<A>\<^sub>i\<^sub>n \<A>\<^sub>i\<^sub>n' x1 x2
    unfolding x P
    by (rule tr[unfolded conc_fun_RETURN])
      (use that in \<open>auto simp: lits_with_max_rel_def dest: in_N0\<close>)

  have H:
  \<open>(replicate (2 * Suc (nat_of_uint32 b)) [], empty_watched \<A>\<^sub>i\<^sub>n)
      \<in> \<langle>Id\<rangle>map_fun_rel ((\<lambda>L. (nat_of_lit L, L)) ` set_mset (\<L>\<^sub>a\<^sub>l\<^sub>l \<A>\<^sub>i\<^sub>n))\<close>
   if \<open>(x, \<A>\<^sub>i\<^sub>n) \<in> ?arg\<close> and
     \<open>x = (a, b)\<close>
    for \<A>\<^sub>i\<^sub>n x a b
    using that unfolding map_fun_rel_def
    by (auto simp: empty_watched_def \<L>\<^sub>a\<^sub>l\<^sub>l_def
        lits_with_max_rel_def
        intro!: nth_replicate dest!: in_N0
        simp del: replicate.simps)
  have initialise_VMTF: \<open>(\<forall>L\<in>#aa. L < b) \<and> distinct_mset aa \<and> (a, aa) \<in>
          \<langle>uint32_nat_rel\<rangle>list_rel_mset_rel \<and> size aa < uint32_max \<Longrightarrow>
        initialise_VMTF a b \<le> RES (isa_vmtf_init aa [])\<close>
    for aa b a
    using initialise_VMTF[of aa, THEN fref_to_Down_curry, of aa b a b]
    by (auto simp: isa_vmtf_init_def conc_fun_RES)
  have [simp]: \<open>(x, y) \<in> \<langle>uint32_nat_rel\<rangle>list_rel_mset_rel \<Longrightarrow> L \<in># y \<Longrightarrow>
     L < Suc (nat_of_uint32 (Max (insert 0 (set x))))\<close>
    for x y L
    by (auto simp: list_rel_mset_rel_def br_def list_rel_def uint32_nat_rel_def
        list_all2_op_eq_map_right_iff' list_mset_rel_def dest: in_N0)

  have initialise_VMTF: \<open>initialise_VMTF a (Suc (nat_of_uint32 b)) \<le>
       \<Down> Id (RES (isa_vmtf_init y []))\<close>
    if \<open>(x, y) \<in> ?arg\<close> and \<open>distinct_mset y\<close> and \<open>length a < uint_max\<close> and \<open>x = (a, b)\<close> for x y a b
    using that
    by (auto simp: P_def lits_with_max_rel_def intro!: initialise_VMTF in_N0)
  have K[simp]: \<open>(x, \<A>\<^sub>i\<^sub>n) \<in> \<langle>uint32_nat_rel\<rangle>list_rel_mset_rel \<Longrightarrow>
         L \<in> atms_of (\<L>\<^sub>a\<^sub>l\<^sub>l \<A>\<^sub>i\<^sub>n) \<Longrightarrow> L < Suc (nat_of_uint32 (Max (insert 0 (set x))))\<close>
    for x L \<A>\<^sub>i\<^sub>n
    unfolding atms_of_\<L>\<^sub>a\<^sub>l\<^sub>l_\<A>\<^sub>i\<^sub>n
    by (auto simp: list_rel_mset_rel_def br_def list_rel_def uint32_nat_rel_def
        list_all2_op_eq_map_right_iff' list_mset_rel_def)
  have cach: \<open>RETURN (replicate (Suc (nat_of_uint32 b)) SEEN_UNKNOWN, [])
      \<le> \<Down> Id
          (SPEC (cach_refinement_empty y))\<close>
    if
      \<open>y = \<A>\<^sub>i\<^sub>n \<and> distinct_mset \<A>\<^sub>i\<^sub>n\<close> and
      \<open>(x, y) \<in> ?arg\<close> and
      \<open>x = (a, b)\<close>
    for M W vm vma \<phi> x y a b
  proof -
    show ?thesis
      unfolding cach_refinement_empty_def RETURN_RES_refine_iff
        cach_refinement_alt_def Bex_def
      by (rule exI[of _ \<open>(replicate (Suc (nat_of_uint32 b)) SEEN_UNKNOWN, [])\<close>]) (use that in
          \<open>auto simp: map_fun_rel_def empty_watched_def \<L>\<^sub>a\<^sub>l\<^sub>l_def
             list_mset_rel_def lits_with_max_rel_def
            simp del: replicate_Suc
            dest!: in_N0 intro: K\<close>)
  qed
  have conflict: \<open>RETURN (True, zero_uint32_nat, replicate (Suc (nat_of_uint32 b)) NOTIN)
      \<le> SPEC (\<lambda>D. (D, None) \<in> option_lookup_clause_rel \<A>\<^sub>i\<^sub>n)\<close>
  if
    \<open>y = \<A>\<^sub>i\<^sub>n \<and> distinct_mset \<A>\<^sub>i\<^sub>n  \<and> isasat_input_bounded \<A>\<^sub>i\<^sub>n\<close> and
    \<open>((a, b), \<A>\<^sub>i\<^sub>n) \<in> lits_with_max_rel O \<langle>uint32_nat_rel\<rangle>mset_rel\<close> and
    \<open>x = (a, b)\<close>
  for a b x y
  proof -
    have \<open>L \<in> atms_of (\<L>\<^sub>a\<^sub>l\<^sub>l \<A>\<^sub>i\<^sub>n) \<Longrightarrow>
        L < Suc (nat_of_uint32 b)\<close> for L
      using that in_N0 by (auto simp: atms_of_\<L>\<^sub>a\<^sub>l\<^sub>l_\<A>\<^sub>i\<^sub>n
          lits_with_max_rel_def)
    then show ?thesis
      by (auto simp: option_lookup_clause_rel_def
      lookup_clause_rel_def simp del: replicate_Suc
      intro: mset_as_position.intros)
  qed
  have [simp]:
     \<open>NO_MATCH 0 a1 \<Longrightarrow> max 0 (Max (insert a1 (set a2))) = max a1 (Max (insert 0 (set a2)))\<close>
    for a1 :: uint32 and a2
    by (metis (mono_tags, lifting) List.finite_set Max_insert all_not_in_conv finite_insert insertI1 insert_commute)
  have le_uint32: \<open>\<forall>L\<in>#\<L>\<^sub>a\<^sub>l\<^sub>l (nat_of_uint32 `# mset a). nat_of_lit L \<le> uint_max \<Longrightarrow>
    Suc (2 * nat_of_uint32 (Max (insert 0 (set a)))) \<le> uint_max\<close> for a
    apply (induction a)
    apply (auto simp: uint32_max_def)
    apply (auto simp: max_def  \<L>\<^sub>a\<^sub>l\<^sub>l_add_mset)
    done
  show ?thesis
    apply (intro frefI nres_relI)
    subgoal for x y
    unfolding init_state_wl_heur_alt_def init_state_wl_D'_def
    apply (rewrite in \<open>let _ = Suc _in _\<close> Let_def)
    apply (rewrite in \<open>let _ = 2 * _in _\<close> Let_def)
    apply (cases x; simp only: prod.case)
    apply (refine_rcg init[of y x] initialise_VMTF cach)
    subgoal for a b by (auto simp: lits_with_max_rel_def intro: le_uint32)
    subgoal by (auto intro!: K[of _ \<A>\<^sub>i\<^sub>n] simp: in_\<L>\<^sub>a\<^sub>l\<^sub>l_atm_of_\<A>\<^sub>i\<^sub>n
     lits_with_max_rel_def atms_of_\<L>\<^sub>a\<^sub>l\<^sub>l_\<A>\<^sub>i\<^sub>n)
    subgoal by auto
    subgoal by auto
    subgoal by auto
    subgoal by (rule conflict)
    subgoal by (rule RETURN_rule) (rule H; simp only:)
         apply assumption
    subgoal by fast
    subgoal by (auto simp: lits_with_max_rel_def P_def)
    subgoal by simp
    subgoal unfolding phase_saving_def lits_with_max_rel_def by (auto intro!: K)
    subgoal by fast
    subgoal by fast
      apply assumption
    apply (rule refl)
    subgoal by (auto simp: P_def init_rll_def option_lookup_clause_rel_def
          lookup_clause_rel_def lits_with_max_rel_def
          simp del: replicate.simps
          intro!: mset_as_position.intros K)
    done
  done
qed


lemma init_state_wl_D':
  \<open>(init_state_wl_D', init_state_wl_heur) \<in>
    [\<lambda>\<A>\<^sub>i\<^sub>n. distinct_mset \<A>\<^sub>i\<^sub>n \<and> isasat_input_bounded \<A>\<^sub>i\<^sub>n]\<^sub>f
      lits_with_max_rel O \<langle>uint32_nat_rel\<rangle>mset_rel \<rightarrow>
      \<langle>Id \<times>\<^sub>r Id \<times>\<^sub>r
         Id \<times>\<^sub>r nat_rel \<times>\<^sub>r \<langle>\<langle>Id\<rangle>list_rel\<rangle>list_rel \<times>\<^sub>r
           Id \<times>\<^sub>r \<langle>bool_rel\<rangle>list_rel \<times>\<^sub>r Id \<times>\<^sub>r Id \<times>\<^sub>r Id \<times>\<^sub>r Id\<rangle>nres_rel\<close>
  apply -
  apply (intro frefI nres_relI)
  by (rule init_state_wl_D0[THEN fref_to_Down, THEN order_trans]) auto

lemma init_state_wl_heur_init_state_wl':
  \<open>(init_state_wl_heur, RETURN o (\<lambda>_. init_state_wl))
  \<in> [\<lambda>N. N = \<A>\<^sub>i\<^sub>n \<and> isasat_input_bounded \<A>\<^sub>i\<^sub>n]\<^sub>f Id \<rightarrow> \<langle>twl_st_heur_parsing_no_WL_wl \<A>\<^sub>i\<^sub>n True\<rangle>nres_rel\<close>
  apply (intro frefI nres_relI)
  unfolding comp_def
  using init_state_wl_heur_init_state_wl[THEN fref_to_Down, of \<A>\<^sub>i\<^sub>n \<open>()\<close> \<open>()\<close>]
  by auto


lemma all_blits_are_in_problem_init_blits_in: \<open>all_blits_are_in_problem_init S \<Longrightarrow> blits_in_\<L>\<^sub>i\<^sub>n S\<close>
  unfolding blits_in_\<L>\<^sub>i\<^sub>n_def
  by (cases S)
   (auto simp: all_blits_are_in_problem_init.simps
    \<L>\<^sub>a\<^sub>l\<^sub>l_atm_of_all_lits_of_mm all_lits_def)

lemma correct_watching_init_blits_in_\<L>\<^sub>i\<^sub>n:
  assumes \<open>correct_watching_init S\<close>
  shows \<open>blits_in_\<L>\<^sub>i\<^sub>n S\<close>
proof -
  show ?thesis
    using assms
    by (cases S)
      (auto simp: all_blits_are_in_problem_init_blits_in
      correct_watching_init.simps)
 qed

fun append_empty_watched where
  \<open>append_empty_watched ((M, N, D, NE, UE, Q), OC) = ((M, N, D, NE, UE, Q, (\<lambda>_. [])), OC)\<close>

fun remove_watched :: \<open>'v twl_st_wl_init_full \<Rightarrow> 'v twl_st_wl_init\<close> where
  \<open>remove_watched ((M, N, D, NE, UE, Q, _), OC) = ((M, N, D, NE, UE, Q), OC)\<close>


definition init_dt_wl' :: \<open>'v clause_l list \<Rightarrow> 'v twl_st_wl_init \<Rightarrow> 'v twl_st_wl_init_full nres\<close> where
  \<open>init_dt_wl' CS S = do{
     S \<leftarrow> init_dt_wl CS S;
     RETURN (append_empty_watched S)
  }\<close>

lemma init_dt_wl'_spec: \<open>init_dt_wl_pre CS S \<Longrightarrow> init_dt_wl' CS S \<le> \<Down>
   ({(S :: 'v twl_st_wl_init_full, S' :: 'v twl_st_wl_init).
      remove_watched S =  S'}) (SPEC (init_dt_wl_spec CS S))\<close>
  unfolding init_dt_wl'_def
  by (refine_vcg  bind_refine_spec[OF _ init_dt_wl_init_dt_wl_spec])
   (auto intro!: RETURN_RES_refine)

lemma init_dt_wl'_init_dt:
  \<open>init_dt_wl_pre CS S \<Longrightarrow> (S, S') \<in> state_wl_l_init \<Longrightarrow> \<forall>C\<in>set CS. distinct C \<Longrightarrow>
  init_dt_wl' CS S \<le> \<Down>
   ({(S :: 'v twl_st_wl_init_full, S' :: 'v twl_st_wl_init).
      remove_watched S =  S'} O state_wl_l_init) (init_dt CS S')\<close>
  unfolding init_dt_wl'_def
  apply (refine_vcg bind_refine[of _ _ _ _ _  \<open>RETURN\<close>, OF init_dt_wl_init_dt, simplified])
  subgoal for S T
    by (cases S; cases T)
      auto
  done

definition isasat_init_fast_slow :: \<open>twl_st_wl_heur_init \<Rightarrow> twl_st_wl_heur_init nres\<close> where
  \<open>isasat_init_fast_slow =
    (\<lambda>(M', N', D', j, W', vm, \<phi>, clvls, cach, lbd, vdom, failed).
      RETURN (trail_pol_slow_of_fast M', N', D', j, convert_wlists_to_nat_conv W', vm, \<phi>,
        clvls, cach, lbd, vdom, failed))\<close>

lemma isasat_init_fast_slow_alt_def:
  \<open>isasat_init_fast_slow S = RETURN S\<close>
  unfolding isasat_init_fast_slow_def trail_pol_slow_of_fast_alt_def
    convert_wlists_to_nat_conv_def
  by auto

end
