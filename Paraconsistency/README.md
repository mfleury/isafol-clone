# Paraconsistency #

[This directory](https://bitbucket.org/isafol/isafol/src/master/Paraconsistency/) contains an Isabelle formalization of
a paraconsistent infinite-valued logic. It extends an [AFP entry](https://www.isa-afp.org/entries/Paraconsistency.html)
and will soon replace it.


## Authors ##

* [Anders Schlichtkrull](mailto:andschl shtrudel dtu.dk)
* [J�rgen Villadsen](mailto:jovi shtrudel dtu.dk)
