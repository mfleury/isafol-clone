# First-Order Logic According to Harrison #

[This directory](https://bitbucket.org/isafol/isafol/src/master/FOL_Harrison/) presents three variants of a certified
declarative first-order prover with equality based on John Harrison's Handbook of Practical Logic and Automated Reasoning,
Cambridge University Press, 2009:

1. A variant characterizing the theorems by an inductive predicate (called OK) describing the proof system. Available as an [AFP entry](https://www.isa-afp.org/entries/FOL_Harrison.shtml).
2. A variant characterizing the theorems by an inductive predicate (called OK) describing the proof system. The predicate is lifted to be a type. Available in the file [FOL_Harrison_Lifting_OK.thy](https://bitbucket.org/isafol/isafol/src/master/FOL_Harrison/FOL_Harrison_Lifting_OK.thy).
3. A variant characterizing the theorems by a predicate describing the truths. The predicate is lifted to be a type. Available in the file [FOL_Harrison_Lifting_Truths.thy](https://bitbucket.org/isafol/isafol/src/master/FOL_Harrison/FOL_Harrison_Lifting_Truths.thy).


## Authors ##

* Variant 1 by [Alexander Birch Jensen](mailto:aleje shtrudel dtu.dk), [Anders Schlichtkrull](mailto:andschl shtrudel dtu.dk) and [Jørgen Villadsen](mailto:jovi shtrudel dtu.dk)
* Variant 2 and 3 by [Anders Schlichtkrull](mailto:andschl shtrudel dtu.dk)
