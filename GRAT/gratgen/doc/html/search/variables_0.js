var searchData=
[
  ['cfg_5fassume_5fnodup',['cfg_assume_nodup',['../gratgen_8cpp.html#a6a1fc387883ae853d2a9e4c763ffd275',1,'gratgen.cpp']]],
  ['cfg_5fbinary_5fdrat',['cfg_binary_drat',['../gratgen_8cpp.html#a8731c61c6e00c0f38a216bfa4bc0c742',1,'gratgen.cpp']]],
  ['cfg_5fcore_5ffirst',['cfg_core_first',['../gratgen_8cpp.html#a696986b1f5e37995e39ab0715b6883cb',1,'gratgen.cpp']]],
  ['cfg_5fignore_5fdeletion',['cfg_ignore_deletion',['../gratgen_8cpp.html#ac5875dc39c8488b4bdd25b917f87c903',1,'gratgen.cpp']]],
  ['cfg_5fno_5fgrat',['cfg_no_grat',['../gratgen_8cpp.html#af1216a3525e4cb1ac8c6204d85c1cfbd',1,'gratgen.cpp']]],
  ['cfg_5fout_5fbuf_5fsize_5fthreshold',['cfg_out_buf_size_threshold',['../gratgen_8cpp.html#a327d6d45361c10e8ae1229d2f6c48645',1,'gratgen.cpp']]],
  ['cfg_5fpremark_5fformula',['cfg_premark_formula',['../gratgen_8cpp.html#aa1c0844166aa054ef2d8f0f4ac6484f3',1,'gratgen.cpp']]],
  ['cfg_5frat_5frun_5fheuristics',['cfg_rat_run_heuristics',['../gratgen_8cpp.html#af2104b53dc3b5740882e347ddb1a2ec2',1,'gratgen.cpp']]],
  ['cfg_5frat_5frun_5freadd_5fupper_5flimit',['cfg_rat_run_readd_upper_limit',['../gratgen_8cpp.html#a3422ef444fb7994ff635bfa560dc0b92',1,'gratgen.cpp']]],
  ['cfg_5fshow_5fprogress_5fbar',['cfg_show_progress_bar',['../gratgen_8cpp.html#a1e80a3342d585369ca148972abcd038c',1,'gratgen.cpp']]],
  ['cfg_5fsingle_5fthreaded',['cfg_single_threaded',['../gratgen_8cpp.html#a1536921f4585fa6622493676da790d7a',1,'gratgen.cpp']]],
  ['cfg_5fsummarize_5fdeletions',['cfg_summarize_deletions',['../gratgen_8cpp.html#a650e5383c9814ec40e3eecaaef234fc7',1,'gratgen.cpp']]]
];
