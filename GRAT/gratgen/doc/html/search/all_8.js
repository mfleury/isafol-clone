var searchData=
[
  ['inc_5frat_5fcounts',['inc_rat_counts',['../classSynch__Data.html#a47bc744917d7f2c1d6cbb5b8d799d555',1,'Synch_Data']]],
  ['init_5fafter_5ffwd',['init_after_fwd',['../classGlobal__Data.html#a364141f1124548842f863980498fe665',1,'Global_Data']]],
  ['init_5fafter_5fparsing',['init_after_parsing',['../classVerifier.html#a53144287275437928552de91c278855f',1,'Verifier']]],
  ['invalid',['INVALID',['../gratgen_8cpp.html#a8c2e74cbe5228c02be4d77db10cce705aef2863a469df3ea6871d640e3669a2f2',1,'gratgen.cpp']]],
  ['is_5fafter_5fparsing',['is_after_parsing',['../classGlobal__Data.html#ad5af9064bb20c8e7a270412e0c6e5b01',1,'Global_Data']]],
  ['is_5fdeletion',['is_deletion',['../classitem__t.html#a9ee7624026a6c4c820e920b9a06a8b88',1,'item_t']]],
  ['is_5ferased',['is_erased',['../classitem__t.html#a5ebe52cc79e3581ad9c4ab6912dafeec',1,'item_t']]],
  ['is_5ffalse',['is_false',['../classVerifier.html#ae00e3ec9b7ef87852b07895ff25e04ed',1,'Verifier']]],
  ['is_5fmarked',['is_marked',['../classSynch__Data.html#a51a6e3c15d0d9997559035a9804dcda1',1,'Synch_Data']]],
  ['is_5ftrue',['is_true',['../classVerifier.html#a460f0231b1a2d1f7fe2adacad567aa00',1,'Verifier']]],
  ['item_5ft',['item_t',['../classitem__t.html',1,'item_t'],['../classitem__t.html#a89f11d6e3bc2a4979b157885988a8bb2',1,'item_t::item_t()']]],
  ['item_5ftype',['item_type',['../gratgen_8cpp.html#a8c2e74cbe5228c02be4d77db10cce705',1,'gratgen.cpp']]],
  ['internal_20storage_20of_20clauses',['Internal storage of clauses',['../pg_clause_format.html',1,'']]]
];
