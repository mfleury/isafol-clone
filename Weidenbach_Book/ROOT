chapter Weidenbach_Book

session "Weidenbach_Book_Base" (Weidenbach_Book) = "Sepref_IICF" +
  description \<open>This session imports HOL, some additional useful theories from HOL, and our own
  extensions above these lemmas.\<close>
  options [document = pdf, document_output = "output"]
  sessions
    "HOL-Eisbach"
    "HOL-Library"
    Nested_Multisets_Ordinals
  theories [document = false]
    "HOL-Library.Code_Target_Numeral"
    "HOL-Library.Multiset"
    "HOL-Library.Multiset_Order"
    "HOL-Library.Extended_Nat"
    Nested_Multisets_Ordinals.Multiset_More
    "HOL-Eisbach.Eisbach"
    "HOL-Eisbach.Eisbach_Tools"
  theories
    Doc_Libraries
    Wellfounded_More
    WB_List_More
    "../lib/Explorer"
  document_files
    "root.tex"
    "biblio.bib"

session "Entailment_Definition" (Weidenbach_Book) = "Weidenbach_Book_Base" +
  description \<open>This session contains various (but still general) definition of entailment used
  later.\<close>
  options [document = pdf, document_output = "output"]
  sessions
    "HOL-Library"
    Ordered_Resolution_Prover
  theories [document = true]
    Doc_Entailment
    Partial_Herbrand_Interpretation
    Partial_Annotated_Herbrand_Interpretation
    Partial_And_Total_Herbrand_Interpretation
    Prop_Logic
  document_files
    "root.tex"
    "biblio.bib"

session "Normalisation" (Weidenbach_Book) = "Entailment_Definition" +
  description \<open>This session contains the normalisation from general formulas to CNF and DNF.\<close>
  options [document = pdf, document_output = "output"]
  sessions
    Nested_Multisets_Ordinals
    Entailment_Definition
  theories [document = false]
    Nested_Multisets_Ordinals.Multiset_More
  theories [document = true]
    Prop_Abstract_Transformation
    Prop_Normalisation
    Prop_Logic_Multiset
  document_files
    "root.tex"
    "biblio.bib"

session "Resolution_Superposition" (Weidenbach_Book) = "Entailment_Definition" +
  description \<open>This session contains the formalisation of resolution and the beginning of
  superposition.\<close>
  options [document = pdf, document_output = "output"]
  theories [document = true]
    Prop_Logic
    Prop_Abstract_Transformation
    Prop_Normalisation
    Prop_Logic_Multiset
    Prop_Resolution
  theories [quick_and_dirty, document = true]
    Prop_Superposition
  document_files
    "root.tex"
    "biblio.bib"

session "CDCL_no_Sepref" (Weidenbach_Book) = "HOL"  +
  description \<open>This session is the same as the CDCL session, but des not import
  the Refinement Framework.
  \<close>
  options [document = pdf, document = false]
  sessions
    Nested_Multisets_Ordinals
    Weidenbach_Book_Base
    Entailment_Definition
    Ordered_Resolution_Prover
  theories [quick_and_dirty = false]
    DPLL_W
    CDCL_W_Level
    CDCL_W
    CDCL_W_Termination
    CDCL_W_Merge
    CDCL_NOT
    CDCL_WNOT
    CDCL_W_Full
    CDCL_W_Restart
    CDCL_W_Incremental

    DPLL_CDCL_W_Implementation
    DPLL_W_Implementation
    CDCL_W_Implementation
    CDCL_Abstract_Clause_Representation
    CDCL_W_Abstract_State

  document_files
    "root.tex"
    "biblio.bib"

session "CDCL" (Weidenbach_Book) = "Sepref_IICF"  +
  description \<open>This session the CDCL calculi (Weidenbach and Nieuwenhuis et al.) and the links between
  both. It also contains a very simple implementation based on lists and two extensions (restart and
  incremental).

  This version is based on the Refinement Framework, to make the two-watched-literals part easier to compile
  (workaround as long as a session cannot have two parents).
  \<close>
  options [document = pdf, document_output = "output"]
  sessions
    Nested_Multisets_Ordinals
    Weidenbach_Book_Base
    Entailment_Definition
  theories [document = false]
    "HOL-Eisbach.Eisbach_Tools"

  theories [quick_and_dirty = false, document = true]
    DPLL_W
    CDCL_W_Level
    CDCL_W
    CDCL_W_Termination
    CDCL_W_Merge
    CDCL_NOT
    CDCL_WNOT
    CDCL_W_Full
    CDCL_W_Restart
    CDCL_W_Incremental

    DPLL_CDCL_W_Implementation
    DPLL_W_Implementation
    CDCL_W_Implementation
    CDCL_Abstract_Clause_Representation
    CDCL_W_Abstract_State

  document_files
    "root.tex"
    "biblio.bib"
  export_files (in "code") [1]
    "CDCL.CDCL_W_Implementation:**"

session "CDCL_Extensions" (Weidenbach_Book) = "CDCL"  +
  description \<open>This sessions contains the extensions of the CDCL calculus.\<close>
  options [document = pdf, document_output = "output"]
  sessions
    "HOL-Library"

  theories [quick_and_dirty = false, document = true]
    CDCL_W_Optimal_Model
    CDCL_W_Partial_Encoding
    CDCL_W_MaxSAT
    CDCL_W_Partial_Optimal_Model
    CDCL_W_Covering_Models

  document_files
    "root.tex"
    "biblio.bib"


session "Watched_Literals" (Weidenbach_Book) = "CDCL"  +
  description \<open>This session contains all theories related to the two-watched literals. It links
    the abstract CDCL calculus to a more concrete version with watched literals.\<close>
  options [document = pdf, document_output = "output"]
  sessions
    CDCL
    Isabelle_LLVM
    "HOL-Eisbach"
  theories [quick_and_dirty = false, document = true]
    Bits_Natural
    IICF_Array_List64
    Array_Array_List64
    IICF_Array_List32
    Array_UInt
    WB_Word
    WB_Word_Assn
    WB_More_Refinement_List
    WB_More_Refinement
    WB_More_IICF_SML
    WB_More_IICF_LLVM
    WB_Sort
    WB_Sort_SML
    Watched_Literals_Transition_System
    Watched_Literals_Algorithm
    Watched_Literals_Algorithm_Restart
    Watched_Literals_List
    Watched_Literals_List_Restart
    Watched_Literals_Watch_List
    Watched_Literals_Watch_List_Restart
    Watched_Literals_Watch_List_Domain
    Watched_Literals_Watch_List_Domain_Restart
    Watched_Literals_Watch_List_Initialisation
    CDCL_Conflict_Minimisation

  document_files
    "root.tex"
    "biblio.bib"


session "IsaSAT" (Weidenbach_Book) = "Watched_Literals"  +
  description \<open>We here refine the previous session to generate code.\<close>
  sessions
    CDCL
    "HOL-Eisbach"
    Watched_Literals
    Isabelle_LLVM
    Show
  theories
    IsaSAT_Arena
    IsaSAT_Arena_SML
    IsaSAT_Clauses
    IsaSAT_Clauses_SML
    IsaSAT_Literals
    IsaSAT_Trail
    Watched_Literals_VMTF
    LBD
    LBD_SML
    Version
    IsaSAT_Watch_List
    IsaSAT_Watch_List_SML
    IsaSAT_Setup
    IsaSAT_Setup_SML
    IsaSAT_Inner_Propagation
    IsaSAT_Inner_Propagation_SML
    IsaSAT_VMTF
    IsaSAT_VMTF_SML
    IsaSAT_Backtrack
    IsaSAT_Backtrack_SML
    IsaSAT_Initialisation
    IsaSAT_Initialisation_SML
    IsaSAT_Conflict_Analysis
    IsaSAT_Conflict_Analysis_SML
    IsaSAT_Propagate_Conflict
    IsaSAT_Propagate_Conflict_SML
    IsaSAT_Decide
    IsaSAT_Decide_SML
    IsaSAT_CDCL
    IsaSAT_CDCL_SML

    IsaSAT_Restart_Heuristics
    IsaSAT_Restart_Heuristics_SML
    IsaSAT_Restart
    IsaSAT_Restart_SML

    IsaSAT
    IsaSAT_SML

  document_files
    "root.tex"
    "biblio.bib"
  export_files (in "code") [1]
    "IsaSAT.IsaSAT:**"

session Model_Enumeration (Weidenbach_Book) = Watched_Literals +
  description \<open>This extends the previous version to a very simple 'SMT' solver: we enumerate all models and let an unspecified theoty solver checks if a model is valid or another must be found.\<close>
  sessions
    Normalisation
    CDCL
    Watched_Literals
  theories[document = true]
    Model_Enumeration
    Watched_Literals_Transition_System_Enumeration
    Watched_Literals_Algorithm_Enumeration
    Watched_Literals_List_Enumeration
    Watched_Literals_Watch_List_Enumeration
  document_files
    "root.tex"
    "biblio.bib"

session Weidenbach_Book (Weidenbach_Book) = IsaSAT +
  description \<open>We import everything\<close>
  options [document = pdf, document_output = "output", quick_and_dirty]
  sessions
    Normalisation
    CDCL
    CDCL_Extensions
    Watched_Literals
    Model_Enumeration
    IsaSAT
    "HOL-Library"
    Resolution_Superposition
  theories
    Weidenbach_Book
  document_files
    "root.tex"
    "biblio.bib"