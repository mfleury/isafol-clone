# Simple Prover #

[This directory](https://bitbucket.org/isafol/isafol/src/master/Simple_Prover/) contains an Isabelle formalization of a certified simple prover for first-order logic based on Tom Ridge's [AFP entry](https://www.isa-afp.org/entries/Verified-Prover.html).

[GitHub](https://github.com/logic-tools/simpro) contains a preliminary version.

A Verified Simple Prover for First-Order Logic.  
J�rgen Villadsen, Anders Schlichtkrull and Andreas Halkj�r From.  
Proceedings of the 6th Workshop on Practical Aspects of Automated Reasoning (PAAR 2018).  
[http://ceur-ws.org/Vol-2162/](http://ceur-ws.org/Vol-2162/)

## Authors ##

* [J�rgen Villadsen](mailto:jovi shtrudel dtu.dk)
* [Anders Schlichtkrull](mailto:andschl shtrudel dtu.dk)
* [Andreas Halkj�r From](mailto:s144442 shtrudel student.dtu.dk)
