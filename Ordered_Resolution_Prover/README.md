# Formalization of Bachmair and Ganzinger's Ordered Resolution Prover #

[This directory](https://bitbucket.org/isafol/isafol/src/master/Ordered_Resolution_Prover/) used to contain the Isabelle formalization of Sections 3 and 4 of Bachmair and Ganzinger's "Resolution Theorem Proving" (Chapter 2 of Volume 1 of _The Handbook of Automated Reasoning_). The formalization has been [moved to the Archive of Formal Proofs](https://www.isa-afp.org/entries/Ordered_Resolution_Prover.shtml). The only exception is the implementation of atoms and substitutions, which is provided using [IsaFoR project](http://cl-informatik.uibk.ac.at/software/ceta/) and has been [moved to the Functional Ordered Resolution Prover IsaFoL entry](https://bitbucket.org/isafol/isafol/src/master/Functional_Ordered_Resolution_Prover/).


## Authors ##

* [Anders Schlichtkrull](mailto:anders shtrudel dtu.dk)
* [Jasmin Christian Blanchette](mailto:jasmin.blanchette shtrudel inria.fr)
* [Dmitriy Traytel](mailto:traytel shtrudel inf.ethz.ch)


## Additional Collaborator ##

* [Uwe Waldmann](mailto:waldmann shtrudel mpi-inf.mpg.de)


## Entry in the Archive of Formal Proofs ##

* [Formalization of Bachmair and Ganzinger's Ordered Resolution Prover](https://www.isa-afp.org/entries/Ordered_Resolution_Prover.shtml)
  A. Schlichtkrull, J. C. Blanchette, D. Traytel, U. Waldmann.
  Archive of Formal Proofs, Formal proof development.
