(*  Title:       Integration of IsaFoR Terms
    Author:      Dmitriy Traytel <traytel at inf.ethz.ch>, 2014
    Author:      Anders Schlichtkrull <andschl at dtu.dk>, 2017
    Maintainer:  Anders Schlichtkrull <andschl at dtu.dk>
*)

section \<open>Integration of IsaFoR Terms\<close>

theory IsaFoR_Term
  imports Main
begin

text \<open>
This part of the formalization has been [moved to the Functional Ordered Resolution Prover AFP entry](https://www.isa-afp.org/browser_info/current/AFP/Functional_Ordered_Resolution_Prover/IsaFoR_Term.html).
\<close>

end
